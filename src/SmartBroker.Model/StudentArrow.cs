using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;


namespace SmartBroker.Model
{
    ///<summary> Пакет для транспорта </summary>
    public class StudentArrow
    {
        ///<summary> Передаваемы список сущностей </summary>
        public StudentEntity[] Students { get; set; }
      
        ///<summary> Идентификатор события или пакета </summary>
        public string EventID  { get; set; }

        ///<summary> X-Request-ID </summary>
        public string RequestID  { get; set; }

        ///<summary> Временная метка из заголовка событий </summary>
        public string EventDateTime { get; set; }

        public StudentArrow()
        {

        }
        
        public StudentArrow(StudentArrow general)
        {
            EventID =  general.EventID;
            RequestID = general.RequestID;
            EventDateTime =  general.EventDateTime;
        }
    }
}
