using System.Collections.Generic;

namespace SmartBroker.Model
{
    /// <summary>
    /// Состояние обработки полученного события 
    /// </summary>
    public class ProcessingStatus
    {
        /// <summary>
        /// Создан, ожидает обработку
        /// </summary>
        public const string Pending = "PENDING";

        /// <summary>
        /// ?
        /// </summary>
        public const string Waring = "WARNING";

        /// <summary>
        /// Завершен с ошибкой
        /// </summary>
        public const string Failed = "FAILED";

        /// <summary>
        /// Завершен успешно
        /// </summary>
        public const string Completed = "COMPLETED";
        
    }
        
}