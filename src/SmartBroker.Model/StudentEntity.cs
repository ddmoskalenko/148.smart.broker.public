﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;


namespace SmartBroker.Model
{
    ///<summary> Сущность сущность для транспорта </summary>
    public class StudentEntity
    {
        ///<summary> Идентификатор сущность </summary>
        public string ID { get; set; }
      
        ///<summary>  Предидущий идентификатор сущность если ID меняется </summary>
        public string IDPrevious { get; set; }
       
        ///<summary> ФормаОбучения </summary>
        public string EductionalForm { get; set; }

        ///<summary> Код Направление Подготовки </summary>
        public string SpecialtyCode { get; set; }

        ///<summary> Наименование направление Подготовки </summary>
        public string Specialty { get; set; }

        ///<summary> Профиль </summary>
        public string Profile { get; set; }

        ///<summary> Причина </summary>
        public string RegistryReason { get; set; }

        ///<summary> ДатаИзменения </summary>
        public string ApplyDate { get; set; }

        ///<summary> Описаине Причины, номер договора </summary>
        public string Ordnung { get; set; }

        ///<summary> ИдентификаторПотокаОбучения </summary>        
        public string FlowID  { get; set; }

        ///<summary> Идентификатор Физическое лицо </summary>
        public string PersonID  { get; set; }
    }
}
