using System.Collections.Generic;

namespace SmartBroker.Model
{
    public enum PacketState
    {
        Ok = 0,
        Bad = 1,
        NotSure = 2
    }
}