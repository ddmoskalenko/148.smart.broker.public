
using System.Collections.Generic;

namespace SmartBroker.Model
{
    /// <summary> Результат обработки события </summary>
    public class EventResult
    {
        /// <summary> Итоговое состояние сообщения в очереди </summary>
        public PacketState PacketState { get; set; }
        /// <summary> Статус состояние сообщения </summary>
        public string ProcessingStatus { get; set; }

        /// <summary> Код ошибки </summary>
        public string ProcessingErrorCode { get; set; }

        /// <summary> Описание результата обработки </summary>}
        public string ProcessingLog { get; set; }
     
        /// <summary> Оканчательное количество поставленных в очередь </summary>}
        public int InQueue  { get; set; }
        
        public EventResult(string messageLog)
        {
            PacketState = PacketState.Bad;
            ProcessingStatus = SmartBroker.Model.ProcessingStatus.Failed;
            ProcessingLog = messageLog;
        }
        public EventResult()
        {
            PacketState = PacketState.Ok;
            ProcessingStatus = SmartBroker.Model.ProcessingStatus.Completed;
        }

        public EventResult(int inQueue)
        {
            PacketState = PacketState.Ok;
            ProcessingStatus = SmartBroker.Model.ProcessingStatus.Completed;
            InQueue = inQueue;
        }
    }
}

