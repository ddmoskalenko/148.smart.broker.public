using System;
using System.Linq;
using System.Collections.Generic;
using System.Xml;
using Dapper;
using Xunit;
using Xunit.Abstractions;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;

namespace SmartBroker.Tests.Tests
{
    /// <summary>Автоматические интеграционные тесты</summary>
    /// <remarks>
    /// Тесты устроены так, что при каждом выполнении любого теста генерируется новый набор 
    /// уникальных идентификаторов 
    /// Отправляются реальные сообщения (шаблонизированные уникальными ID) в реальный RabbitMQ на стенде,
    /// и далее тесты смотрят, что видно в разных БД через какое-то время.
    /// Также иногда вызывается синхронный API для отмены/подтверждения операций.
    /// </remarks>
    public class StudentMovementsTests : IntegrationTestBase
    {
        #region Инфраструктура тестов 

        private class Db
        {
        }

        /// <summary>Номер прогона полного теста, определяется по таблице физлиц </summary>
        /// <remarks>
        /// Подставляется в шаблоны сообщений как {TestRun#}
        /// </remarks>
        private int TestRunNumber { get; set; }

        /// <summary>Случайное число 0x1..0xffff для одного запуска</summary>
        /// <remarks>
        /// Подставляется в шаблоны сообщений как {Random#}
        /// </remarks>
        private int RandomNumber { get; set; }

        // Если вдруг какие-то проблемы, можно подвинуть вручную вперед
        private const int MIN_TEST_RUN_NUMBER = 400;

        private IRemoteRestApi StudentsControllerRestApi { get; }

        /// <summary>Конструктор</summary>
        public StudentMovementsTests(ITestOutputHelper output) : base(output)
        {
            // Чтобы Dapper сам переводил snake_case в PascalCase
            DefaultTypeMap.MatchNamesWithUnderscores = true;

            TestRunNumber = Math.Max((FindLastUsedTestRunNumber() ?? 0) + 1, MIN_TEST_RUN_NUMBER);
            Logger.Information("New test run number: {TestRunNumber} ('...-{TestRunNumberHex}-...')",
                TestRunNumber, $"{TestRunNumber:x4}");

            RandomNumber = new Random().Next(0, 0xFFFF);
            Logger.Information("New random number: {RandomNumber} ('...-{RandomNumberHex}-...')",
                RandomNumber, $"{RandomNumber:x4}");

            StudentsControllerRestApi = RemoteRestApiFactory.Create(
                new RestApiConnectionOptions
                {
                    Url = Configuration["StudentMovementTests:RestApi:StudentsControllerBaseApiUrl"],
                    RequestTimeout = TimeSpan.FromSeconds(30)
                });
        }

        private string GetNotNullCfg(string key) => Configuration[key]
            ?? throw new InvalidOperationException($"В конфигурации не задано значение '{key}'");

        private string RoutingKey => GetNotNullCfg("StudentMovementTests:MessageSending:RoutingKey");
        private string ExchangeName => GetNotNullCfg("StudentMovementTests:MessageSending:ExchangeName");

        private string GetTestMessageValue(string fileName, string xPath)
            => GetTestMessageXml(fileName).SelectSingleNode(xPath)?.Value;

        private IDictionary<string, string> GetTestMessageElementAttributes(string fileName, string elementXPath)
            => GetTestMessageXml(fileName).SelectSingleNode(elementXPath)
                .Attributes.OfType<XmlAttribute>()
                .ToDictionary(a => a.Name, a => a.Value);

        private XmlDocument GetTestMessageXml(string fileName)
        {
            var msg = PrepareTestMessage(GetTestMessage(fileName));
            var xml = new XmlDocument();
            xml.LoadXml(msg);
            return xml;
        }

        private void SendMessage(string messageId, string exchangeName, string body, Dictionary<string, string> headers)
            => MessageSender.SendMessage(messageId, exchangeName, RoutingKey, body, "application/xml", headers);

        private string PrepareTestMessage(string msg)
            => msg.Replace("{TestRun#}", string.Format("{0:x4}", TestRunNumber))
                .Replace("{Random#}", string.Format("{0:x4}", RandomNumber));

        private int? FindLastUsedTestRunNumber()
        {
            using (var db = GetDbConnection(Db.Persons))
            {
                // Тестовые физики имеют в именах и фамилиях суффикс вида "-nnnn-L"
                string sql = @"
                    SELECT max(num)
                    FROM
                    (
                        SELECT ('x' || (regexp_matches(surname, '-([0-9a-f]{4})-[A-Z]$'))[1])::bit(16)::int AS num
                        FROM person AS p
                        WHERE p.surname SIMILAR TO '%-[0-9a-f]{4}-[A-Z]'
                    ) nums
                ";

                return db.QuerySingle<int?>(sql);
            }
        }

        private void StudentsControllerApiPost(string relativeUrl, object body)
        {
            var json = JsonConvert.SerializeObject(body);
            var content = new StringContent(json, Encoding.UTF8, RestApiHelper.JsonContentType);
            StudentsControllerRestApi.Post(relativeUrl, content);
        }

        private void StudentsControllerApiGet(string relativeUrl)
        {
            StudentsControllerRestApi.Get<dynamic>(relativeUrl);
        }

        #endregion

        #region Отдельные операции для тестов

        private void SendStudentMovementMessage(string fileName)
        {
            var msg = PrepareTestMessage(GetTestMessage(fileName));

            var eventType = GetTestMessageValue(fileName, "Событие/@ТипСобытия");
            var eventId = GetTestMessageValue(fileName, "Событие/@Идентификатор");

            SendMessage(eventId, ExchangeName, msg, new Dictionary<string, string> { { "Modeus.EventType", eventType } });

            Logger.Information("Отправлено тестовое сообщение {TestMessageFileName} ({EventType}), id: {EventId}", fileName, eventType, eventId);

            AssertThatEventually(() => HasMessageReceived(eventId), stateDescription: "Сообщение получено и сохранено в БД адаптера (raw_msg + feed)");
            AssertMessageIsCorrect(eventId);
        }

        private bool IsTestPersonExists(int? version = null)
            => GetTestPersonsOrdered(version).Any();

        private char GetLastLetter(string s) => s.Substring(s.Length - 1, 1)[0];

        /// <summary>Возвращает перечень тестовых физлиц, отсортированный по последней букве имени</summary>
        /// <remarks>
        /// Последняя буква имени в тестах - A, B, C, etc - это удобно для сопоставления с тестовыми сообщениями
        /// </remarks>
        private IList<Person> GetTestPersonsOrdered(int? version = null)
        {
            string sql = @"
                    SELECT *
                    FROM person AS p
                    WHERE p.surname SIMILAR TO '%-' || :numHex  || '-[A-Z]'
                ";

            if (version.HasValue)
                sql += " AND version = :version";

            using (var db = GetDbConnection(Db.Persons))
            {
                var result = db.Query<Person>(sql, new { numHex = $"{TestRunNumber:x4}", version })
                    .OrderBy(p => GetLastLetter(p.Surname))
                    .ToList();

                Logger.Information("Физлиц с тестовым номером {TestRunNumber} (версия {Version}) найдено: {Count}",
                    TestRunNumber,
                    version?.ToString() ?? "*",
                    result.Count);

                return result;
            }
        }

        private bool IsCorrectGender(string gender) => gender == "MALE" || gender == "FEMALE";

        private void AssertThatGenderIsCorrect(string gender)
            => Assert.True(IsCorrectGender(gender), $"Invalid gender value: {gender}");

        /// <summary>Возвращает список студентов данного тестового прогона с сортировкой по последней букве имени</summary>
        /// <remarks>
        /// Последняя буква имени в тестах - A, B, C, etc - это удобно для сопоставления с тестовыми сообщениями
        /// </remarks>
        private IList<Student> GetTestStudentsOrdered(IList<Person> testPersons)
        {
            var ids = testPersons.Select(p => p.Id).ToList();
            var personNameLastLetters = testPersons.ToDictionary(
                p => p.Id,
                p => GetLastLetter(p.Surname));

            string sql = @"
                    SELECT *
                    FROM student AS s
                    WHERE s.person_id = ANY(:ids)
                ";

            using (var db = GetDbConnection(Db.Students))
            {
                return db.Query<Student>(sql, new { ids })
                    .OrderBy(s => personNameLastLetters[s.PersonId])
                    .ToList();
            }
        }

        private (RawMessage, Feed)? FindReceivedMessage(string eventId)
        {
            using (var db = GetDbConnection(Db.StudentAdapter))
            {
                var msg = db.QuerySingleOrDefault<RawMessage>(
                    $@"
                        SELECT msg.*
                        FROM student_feed_raw_message msg
                        JOIN student_feed_message_log feed ON msg.feed_id = feed.id
                            WHERE msg.message_id = :eventId",
                    new { eventId });

                var feed = db.QuerySingleOrDefault<Feed>(
                    $@"
                        SELECT feed.* 
                        FROM student_feed_raw_message msg
                        JOIN student_feed_message_log feed ON msg.feed_id = feed.id
                            WHERE msg.message_id = :eventId",
                    new { eventId });

                return (msg != null && feed != null) 
                    ? (msg, feed) 
                    : ((RawMessage, Feed)?)null;
            }
        }

        private bool HasMessageReceived(string eventId) => FindReceivedMessage(eventId) != null;

        private void AssertMessageIsCorrect(string eventId)
        {
            var msgAndFeed = FindReceivedMessage(eventId);

            TreatAsWarning(() => Assert.True(msgAndFeed != null, $"Не найдена запись сообщения {eventId} (raw_msg + feed)"));

            if (msgAndFeed.HasValue)
            {
                (var msg, var feed) = msgAndFeed.Value;

                if (msg != null)
                {
                    TreatAsWarning(() => Assert.True(msg.AudCreatedBy != null, $"Не заполнен автор записи сообщения (raw_msg)"));
                    TreatAsWarning(() => Assert.True(msg.AudCreatedTs != null, $"Не заполнена дата создания записи сообщения (raw_msg)"));
                    TreatAsWarning(() => AssertIsAlmostNowUtc(msg.AudCreatedTs, valueSource: "raw_msg.AudCreatedTs"));
                }

                if (feed != null)
                {
                    TreatAsWarning(() => Assert.True(feed.AudCreatedBy != null, $"Не заполнен автор записи сообщения (feed)"));
                    TreatAsWarning(() => Assert.True(feed.AudCreatedTs != null, $"Не заполнена дата создания записи сообщения (feed)"));

                    TreatAsWarning(() => AssertIsAlmostNowUtc(feed.AudCreatedTs, valueSource: "feed.AudCreatedTs"));
                    TreatAsWarning(() => AssertIsAlmostNowUtc(feed.DeliveryTimestamp, valueSource: "feed.DeliveryTimestamp"));

                    TreatAsWarning(() => Assert.False(string.IsNullOrWhiteSpace(feed.FeedType)));
                    TreatAsWarning(() => Assert.False(string.IsNullOrWhiteSpace(feed.ProcessingStatus)));
                }
            }
        }

        private void AssertPersonOrStudentMappingExistsAndCorrect(string mappingTableName, string extId, string intId)
        {
            using (var db = GetDbConnection(Db.StudentAdapter))
            {
                var mapping = db.QuerySingleOrDefault<PersonOrStudentMapping>(
                    $@"SELECT * FROM {mappingTableName}
                            WHERE id = :intId AND external_id = :extId",
                    new { intId, extId });

                var msgSuffix = $"в таблице {mappingTableName}: {extId} -> {intId}";

                TreatAsWarning(() => Assert.True(mapping != null, $"Не найдена запись маппинга {msgSuffix}"));
                if (mapping != null)
                {
                    TreatAsWarning(() => Assert.Null(mapping.AudLastUpdatedBy));
                    TreatAsWarning(() => Assert.Null(mapping.AudLastUpdatedTs));

                    TreatAsWarning(() => Assert.True(mapping.AudCreatedBy != null, $"Не заполнен автор записи маппинга {msgSuffix}"));
                    TreatAsWarning(() => Assert.True(mapping.AudCreatedTs != null, $"Не заполнена дата создания записи маппинга {msgSuffix}"));
                    TreatAsWarning(() => Assert.True(mapping.ExternalEventType != null, $"Не заполнен типа внешнего события в записи маппинга {msgSuffix}"));
                    TreatAsWarning(() => Assert.True(mapping.ExternalEventTime != null, $"Не заполнено время внешнего события в записи маппинга {msgSuffix}"));

                    TreatAsWarning(() => AssertIsAlmostNowUtc(mapping.AudCreatedTs, valueSource: $"{mappingTableName}.AudCreatedTs"));
                    TreatAsWarning(() => AssertIsAlmostNowUtc(mapping.ExternalEventTime, valueSource: $"{mappingTableName}.ExternalEventTime"));
                }
            }
        }

        private Event GetEventByIdOrNull(string eventId)
        {
            var sql = @"SELECT * FROM ""event"" WHERE id = :eventId";

            using (var db = GetDbConnection(Db.Students))
            {
                var result = db.QuerySingleOrDefault<Event>(sql, new { eventId });
                return result;
            }
        }

        private Event GetEventById(string eventId)
        {
            var result = GetEventByIdOrNull(eventId);
            Assert.True(result != null, $"Не найдена запись о событии с id {eventId}");
            return result;
        }

        private string GetEventId(string messageFileName) 
            => GetTestMessageValue(messageFileName, "Событие/@Идентификатор");

        private DateTimeOffset GetEventDateTime(string messageFileName) 
            => DateTimeOffset.Parse(GetTestMessageValue(messageFileName, "Событие/@ДатаВремяСобытия"));

        private Event GetEventByTestMessage(string messageFileName) 
            => GetEventById(GetEventId(messageFileName));

        private void AssertEventIsCorrect(Event evt, string expectedOpType, DateTimeOffset expectedDateTime)
        {
            WarningAssertThat(evt.OperationTypeId == expectedOpType, 
                $"У записи события {evt.Id} тип операции не совпадает с ожидаемым: {expectedOpType}");

            // Трактуем время в БД как записанное в UTC
            var actualDateTimeUtc = DateTime.SpecifyKind(evt.EventDateTimeUtc.Value, DateTimeKind.Utc);

            // Приведем для сравения время из сообщения тоже в UTC
            var expectedDateTimeUtc = DateTime.SpecifyKind(expectedDateTime.ToUniversalTime().DateTime, DateTimeKind.Utc);

            WarningAssertThat(actualDateTimeUtc == expectedDateTimeUtc, 
                $"У записи события {evt.Id} значение момента события в БД в UTC ({actualDateTimeUtc}) " +
                $"не совпадает с ожидаемым: {expectedDateTimeUtc}. " +
                $"Исходное дата и время с указанием часового пояса: {expectedDateTime}");
        }

        private Event GetEventWithCorrectnessAssertion(string eventId, string expectedOpType, DateTimeOffset expectedDateTime)
        {
            var evt = GetEventById(eventId);
            AssertEventIsCorrect(evt, expectedOpType, expectedDateTime);
            return evt;
        }

        private bool IsEventExists(string eventId) => GetEventByIdOrNull(eventId) != null;

        private IList<Operation> GetOperations(string eventId)
        {
            var sql = @"SELECT * FROM ""operation"" WHERE event_id = :eventId";

            using (var db = GetDbConnection(Db.Students))
            {
                var result = db.Query<Operation>(sql, new { eventId }).ToList();
                return result;
            }
        }

        private Operation GetSingleOperation(string eventId)
        {
            var result = GetOperations(eventId);
            Assert.True(result.Count != 0, $"Не найдено записей операций с event_id {eventId}");
            Assert.True(result.Count == 1, $"Найдено больше одной записи с event_id {eventId}");
            return result.Single();
        }

        private bool AllOperationsHasStatus(string eventId, params string[] statuses) 
            => GetOperations(eventId).All(op => statuses.Contains(op.StatusId));

        private FlowMapping GetFlowMapping(string internalId)
        {
            var mapping = GetFlowMappingOrNull(internalId);
            Assert.NotNull(mapping);
            return mapping;
        }

        private FlowMapping GetFlowMappingOrNull(string internalId)
        {
            using (var db = GetDbConnection(Db.StudentAdapter))
            {
                var mapping = db.QuerySingleOrDefault<FlowMapping>(
                    $@"SELECT * FROM curriculum_flow_mapping
                            WHERE id = :internalId",
                    new { internalId });

                return mapping;
            }
        }

        private void AssertFlowMappingExistsAndCorrect(string internalId, string externalId)
        {
            var mapping = GetFlowMappingOrNull(internalId);
            TreatAsWarning(() => Assert.True(mapping != null, $"Запись маппинга потока с intId: {internalId} не найдена"));
            if (mapping != null)
            {
                TreatAsWarning(() => Assert.True(externalId == mapping.ExternalId, 
                    $"В маппинге с intId: {internalId} указан неверный extId: {mapping.ExternalId} вместо {externalId}"));

                TreatAsWarning(() => Assert.True(mapping.AudCreatedBy != null, $"Не заполнен автор записи маппинга потока с intId: {internalId}"));
                TreatAsWarning(() => Assert.True(mapping.AudCreatedTs != null, $"Не заполнена дата создания записи маппинга потока с intId: {internalId}"));
            }
        }

        #endregion

        #region Тест механизмов XPath

        [Fact]
        public void Методы_с_XPath_работают()
        {
            Assert.Equal("НовыйСтудент", GetTestMessageValue("Новые_студенты", "Событие/@ТипСобытия"));
            Assert.Equal("История", GetTestMessageValue("Новые_студенты", "Событие/Люди/Человек[2]/Студент/@НаправлениеПодготовки"));

            var attrs = GetTestMessageElementAttributes("Новые_студенты", "Событие");
            Assert.Equal("НовыйСтудент", attrs["ТипСобытия"]);
        }

        #endregion

        #region Длинный связанный тест

        [Fact]
        public void Длинный_тестовый_сценарий_успешно_работает()
        {
            Создание_пары_физлиц();
            Logger.Information("Успешно проверено: создание физлиц");

            Изменение_пары_физлиц();
            Logger.Information("Успешно проверено: изменение физлиц");

            Создание_пары_студентов();
            Logger.Information("Успешно проверено: создание студентов");

            Перевод_студента_внутри_потока();
            Logger.Information("Успешно проверено: перевод студента внутри потока");

            Перевод_студента_в_другой_поток();
            Logger.Information("Успешно проверено: перевод студента в другой поток");

            Отчисление_студента();
            Logger.Information("Успешно проверено: отчисление");

            Отмена_зачисления_студента();
            Logger.Information("Успешно проверено: отмена зачисления");

            Откат_отмены_зачисления();
            Logger.Information("Успешно проверено: откат отмены зачисления");

            AssertWereNoWarnings();
        }

        private void Создание_пары_физлиц()
        {
            Assert.False(IsTestPersonExists());

            SendStudentMovementMessage("Новые_физлица");

            AssertThatEventually(() => GetTestPersonsOrdered(version: 0).Count == 2,
                stateDescription: "Есть два тестовых физлица версии 0");

            var persons = GetTestPersonsOrdered();

            Assert.Equal(2, persons.Count);

            foreach (var person in persons)
            {
                TreatAsWarning(() => Assert.Equal(0, person.Version));
                TreatAsWarning(() => Assert.False(person.IsArchived));
                TreatAsWarning(() => Assert.Equal(TimeSpan.Zero, person.Birthdate.TimeOfDay));

                TreatAsWarning(() => Assert.Null(person.AudLastUpdatedBy));
                TreatAsWarning(() => Assert.Null(person.AudLastUpdatedTs));
                TreatAsWarning(() => Assert.NotEqual(default, person.AudCreatedTs));
                TreatAsWarning(() => Assert.NotNull(person.AudCreatedBy));
                TreatAsWarning(() => AssertIsAlmostNowUtc(person.AudCreatedTs, valueSource: "person.AudCreatedTs"));

                TreatAsWarning(() => AssertThatGenderIsCorrect(person.Gender));

                // Проверим маппинг
                var n = persons.IndexOf(person) + 1;
                var extId = GetTestMessageValue("Новые_физлица", $"Событие/Люди/Человек[{n}]/@Идентификатор");
                var intId = person.Id;
                AssertPersonOrStudentMappingExistsAndCorrect("person_mapping", extId, intId);

                // TODO: Проверить еще контактные данные

                Logger.Information("Проверено физлицо {FullName}", person.FullName);
            }
        }

        private void Изменение_пары_физлиц()
        {
            Assert.True(IsTestPersonExists());

            SendStudentMovementMessage("Изменение_физлиц");

            AssertThatEventually(() => IsTestPersonExists(version: 1),
                stateDescription: "Есть тестовые физлица версии 1");

            var persons = GetTestPersonsOrdered();

            Assert.Equal(2, persons.Count);

            foreach (var person in persons)
            {
                Assert.Equal(1, person.Version);

                TreatAsWarning(() => Assert.NotEqual(default, person.AudCreatedTs));
                TreatAsWarning(() => Assert.NotNull(person.AudCreatedBy));
                TreatAsWarning(() => Assert.NotNull(person.AudLastUpdatedBy));
                TreatAsWarning(() => Assert.NotNull(person.AudLastUpdatedTs));
                TreatAsWarning(() => Assert.NotEqual(person.AudLastUpdatedTs, person.AudCreatedTs));

                // TODO: Проверить контактные данные

                TreatAsWarning(() => AssertIsAlmostNowUtc(person.AudLastUpdatedTs, valueSource: "person.AudLastUpdatedTs"));
                TreatAsWarning(() => AssertThatGenderIsCorrect(person.Gender));
            }

            Assert.Equal("MALE", persons[0].Gender);
            Assert.Equal("FEMALE", persons[1].Gender);
        }

        private void Создание_пары_студентов()
        {
            Assert.True(IsTestPersonExists());

            var persons = GetTestPersonsOrdered();

            SendStudentMovementMessage("Новые_студенты");

            AssertThatEventually(() => GetTestStudentsOrdered(persons).Count == persons.Count,
                stateDescription: $"Есть ({persons.Count}) тестовых студентов");

            // Направления появляются позже
            AssertThatEventually(() => GetTestStudentsOrdered(persons).All(s => !string.IsNullOrEmpty(s.SpecialtyCode)),
                stateDescription: $"У тестовых студентов проставлены направления");

            var students = GetTestStudentsOrdered(persons);
            Assert.Equal(persons.Count, students.Count);

            foreach (var student in students)
            {
                var n = students.IndexOf(student) + 1;
                var testAttrs = GetTestMessageElementAttributes("Новые_студенты", $"Событие/Люди/Человек[{n}]/Студент");

                Assert.Equal(
                    testAttrs["НаправлениеПодготовкиКод"],
                    student.SpecialtyCode);

                Assert.Equal(testAttrs["НаправлениеПодготовки"], student.SpecialtyName);
                Assert.Equal(testAttrs["Профиль"], student.SpecialtyProfile);
                Assert.Equal(DateTime.Parse(testAttrs["ДатаИзменения"]), student.LearningStartDate);
                Assert.False(student.Deleted);

                // Проверим маппинги
                AssertPersonOrStudentMappingExistsAndCorrect("student_mapping", testAttrs["Идентификатор"], student.Id);
                AssertFlowMappingExistsAndCorrect(student.CurriculumFlowId, testAttrs["ИдентификаторПотокаОбучения"]);

                // TODO: Проверить еще остальные поля
            }

            // Проверка, что не натолкали в БД студентов кодов направлений неверного формата:
            {
                // Не должно быть точек в конце
                var sql = @"
                    SELECT s.specialty_code
                    FROM student AS s
                    WHERE specialty_code LIKE '%.'
                    GROUP BY s.specialty_code
                ";
                using (var db = GetDbConnection(Db.Students))
                    TreatAsWarning(() => Assert.True(db.Query<string>(sql).IsEmpty(), "В БД студентов есть коды направлений с точками на конце"));
            }

            // Проверка, что не натолкали к БД студентов разных названий направлений на один код:
            {
                var sql = @"
                    SELECT s.specialty_code
                    FROM student AS s
                    GROUP BY s.specialty_code
                    HAVING count(DISTINCT s.specialty_name) > 1
                ";
                using (var db = GetDbConnection(Db.Students))
                    Assert.True(db.Query<string>(sql).IsEmpty(), "В БД студентов есть разные названия направлений на один код");
            }

            // Проверка записи события
            var eventId = GetEventId("Новые_студенты");
            AssertThatEventually(() => IsEventExists(eventId),
                stateDescription: $"Запись события о новых студентах заведена");

            var evt = GetEventWithCorrectnessAssertion(eventId, Event.STUDENT_ENROLLMENT, GetEventDateTime("Новые_студенты"));

            // Проверка успешности операции
            AssertThatEventually(() => AllOperationsHasStatus(eventId, Operation.SUCCESSFUL),
                stateDescription: $"Все операции создания студентов успешно завершены");

            // TODO: Проверка БД планирования
        }

      

        private void Перевод_студента_внутри_потока()
        {
            Assert.True(IsTestPersonExists());
            var persons = GetTestPersonsOrdered();

            var newSpecialityCode = GetTestMessageValue("Перевод_студента_внутри_потока", "Событие/Люди/Человек/Студент/@НаправлениеПодготовкиКод");

            var oldStudents = GetTestStudentsOrdered(persons);
            Assert.DoesNotContain(newSpecialityCode, oldStudents.Select(s => s.SpecialtyCode));

            SendStudentMovementMessage("Перевод_студента_внутри_потока");

            AssertThatEventually(() => GetTestStudentsOrdered(persons).Any(s => s.SpecialtyCode == newSpecialityCode),
                stateDescription: $"У студента измененный код направления ({newSpecialityCode})");

            // Проверка записи события
            var eventId = GetEventId("Перевод_студента_внутри_потока");
            AssertThatEventually(() => IsEventExists(eventId),
                stateDescription: $"Запись события о переводе заведена");

            var evt = GetEventWithCorrectnessAssertion(eventId, Event.CHANGE_PROFILE, GetEventDateTime("Перевод_студента_внутри_потока"));

            // Проверка успешности операции
            AssertThatEventually(() => AllOperationsHasStatus(eventId, Operation.SUCCESSFUL),
                stateDescription: $"Операция перевода студента внутри потока успешно завершена");

            // TODO: Проверка БД планирования
        }

        private void Перевод_студента_в_другой_поток()
        {
            Assert.True(IsTestPersonExists());
            var persons = GetTestPersonsOrdered();
            var person = persons.Last();
            var studentAttrs = GetTestMessageElementAttributes("Перевод_студента_в_другой_поток", "Событие/Люди/Человек/Студент");
            var eventAttrs = GetTestMessageElementAttributes("Перевод_студента_в_другой_поток", "Событие");
            var eventDate = DateTimeOffset.Parse(eventAttrs["ДатаВремяСобытия"]).Date;
            var changeDate = DateTimeOffset.Parse(studentAttrs["ДатаИзменения"]).Date;

            // Сперва есть только один студент с этим personId
            var oldStudent = GetTestStudentsOrdered(new[] { person }).Single();
            TreatAsWarning(() => Assert.Equal(studentAttrs["ИдентификаторПотокаОбученияПредыдущий"], 
                GetFlowMapping(oldStudent.CurriculumFlowId).ExternalId));
            Assert.Null(oldStudent.LearningEndDate);

            // Отправляем ообщение о переводе
            SendStudentMovementMessage("Перевод_студента_в_другой_поток");

            // Должно стать два студента (один - отчисленный переводом)
            AssertThatEventually(() => GetTestStudentsOrdered(new[] { person }).Count == 2, 
                stateDescription: "После перевода на другой поток студент раздвоился");

            // Зачитаем заново старого и нового 
            var oldStudentBackup = oldStudent;
            oldStudent = GetTestStudentsOrdered(new[] { person }).Where(s => s.Id == oldStudent.Id).Single();
            var newStudent = GetTestStudentsOrdered(new[] { person }).Where(s => s.Id != oldStudent.Id).Single();

            // Проверим, что в старом студенте ничего лишнего не поменялось
            TreatAsWarning(() => Assert.True(oldStudentBackup.CurriculumFlowId == oldStudent.CurriculumFlowId, "У старого студента изменился поток"));
            TreatAsWarning(() => Assert.True(oldStudentBackup.Deleted == oldStudent.Deleted, "У старого студента изменился признак удаления"));
            TreatAsWarning(() => Assert.True(oldStudentBackup.LearningStartDate == oldStudent.LearningStartDate, "У старого студента изменился признак удаления"));
            TreatAsWarning(() => Assert.True(oldStudentBackup.SpecialtyCode == oldStudent.SpecialtyCode, "У старого студента изменился код НП"));
            TreatAsWarning(() => Assert.True(oldStudentBackup.SpecialtyName == oldStudent.SpecialtyName, "У старого студента изменилось имя НП"));
            TreatAsWarning(() => Assert.True(oldStudentBackup.SpecialtyProfile == oldStudent.SpecialtyProfile, "У старого студента изменился профиль"));
            TreatAsWarning(() => Assert.True(oldStudentBackup.Version + 1 == oldStudent.Version, "У старого студента некорректно проставлена версия"));

            // А что нужно - поменялось
            TreatAsWarning(() => Assert.True(oldStudent.LearningEndDate != null, "В старом студенте не проставлена дата отчисления"));
            TreatAsWarning(() => Assert.True(changeDate == oldStudent.LearningEndDate.Value.Date,
                $"В старом студенте проставлена неверная дата отчисления: {oldStudent.LearningEndDate.Value.Date} вместо {changeDate}"));
            TreatAsWarning(() => Assert.True(oldStudent.LearningEndReason != null, "В старом студенте не указана причина отчисления"));

            // TODO: проверить причину отчисления

            // Проверим потоки
            var oldStudentFlowMapping = GetFlowMapping(oldStudent.CurriculumFlowId);
            var newStudentFlowMapping = GetFlowMapping(newStudent.CurriculumFlowId);
            TreatAsWarning(() => Assert.True(studentAttrs["ИдентификаторПотокаОбученияПредыдущий"] == oldStudentFlowMapping.ExternalId,
                $"Поток старого студента некорректный: ${oldStudentFlowMapping.ExternalId} вместо {studentAttrs["ИдентификаторПотокаОбученияПредыдущий"]}"));
            TreatAsWarning(() => Assert.True(studentAttrs["ИдентификаторПотокаОбучения"] == newStudentFlowMapping.ExternalId,
                $"Поток нового студента некорректный: ${newStudentFlowMapping.ExternalId} вместо {studentAttrs["ИдентификаторПотокаОбучения"]}"));               

            // Проверка записи события
            var eventId = GetEventId("Перевод_студента_в_другой_поток");
            AssertThatEventually(() => IsEventExists(eventId),
                stateDescription: $"Запись события о переводе заведена");

            var evt = GetEventWithCorrectnessAssertion(eventId, Event.CHANGE_PROFILE, GetEventDateTime("Перевод_студента_в_другой_поток"));

            // Проверка успешности операции
            AssertThatEventually(() => AllOperationsHasStatus(eventId, Operation.SUCCESSFUL, Operation.WARNING),
                stateDescription: $"Операция перевода студента в другой поток успешно завершена");

            // TODO: Проверка БД планирования
        }

        private void Отчисление_студента()
        {
            const string EVENT_NAME = "Отчисление";
            Assert.True(IsTestPersonExists());
            var persons = GetTestPersonsOrdered();
            var students = GetTestStudentsOrdered(persons);

            SendStudentMovementMessage(EVENT_NAME);

            // Проверка записи события
            var eventId = GetEventId(EVENT_NAME);
            AssertThatEventually(() => IsEventExists(eventId),
                stateDescription: $"Запись события об отчислении заведена");

            var evt = GetEventWithCorrectnessAssertion(eventId, Event.EXPULSION, GetEventDateTime(EVENT_NAME));

            // Проверка статуса операции
            AssertThatEventually(() => AllOperationsHasStatus(eventId, Operation.SUCCESSFUL),
                stateDescription: $"Операция отчисления выполнена успешно");

            AssertThatEventually(() => GetTestStudentsOrdered(persons).Where(s => s.LearningEndDate == null).Count() == 1,
                stateDescription: $"После отчисления остался только один студент без даты окончания");

            // TODO: Проверка БД планирования
        }

        private void Отмена_зачисления_студента()
        {
            Assert.True(IsTestPersonExists());
            var persons = GetTestPersonsOrdered();
            var students = GetTestStudentsOrdered(persons);

            SendStudentMovementMessage("Отмена_зачисления");

            // Проверка записи события
            var eventId = GetEventId("Отмена_зачисления");
            AssertThatEventually(() => IsEventExists(eventId),
                stateDescription: $"Запись события об отмене зачисления заведена");

            var evt = GetEventWithCorrectnessAssertion(eventId, Event.CANCEL_ENROLLMENT, GetEventDateTime("Отмена_зачисления"));
            
            // Проверка статуса операции
            AssertThatEventually(() => AllOperationsHasStatus(eventId, Operation.SUSPENDING),
                stateDescription: $"Операция отмены зачисления остановлена");

            // TODO: Проверка БД планирования
        }

        private void Откат_отмены_зачисления()
        {
            // Проверка статуса операции
            var eventId = GetEventId("Отмена_зачисления");
            Assert.Equal(Operation.SUSPENDING, GetSingleOperation(eventId).StatusId);

            // Вызываем через API откат операции
            StudentsControllerApiPost("operation/canceling-operation", body: new[] { GetSingleOperation(eventId).Id });

            // Изменение должен быть синхронным с получением ответа от REST API!
            Assert.True(GetSingleOperation(eventId).StatusId == Operation.CANCELED, 
                "Операция отмены зачисления не отменена синхронно с вызовом REST API");

            // Проверка статуса студентов
            var persons = GetTestPersonsOrdered();
            AssertThatEventually(() => GetTestStudentsOrdered(persons).Where(s => s.LearningEndDate == null).Count() == 1,
                stateDescription: $"После отката отчисления остался только один студент без даты окончания");

            // Проверка неудаленности обоих студентов
            Assert.True(GetTestStudentsOrdered(persons).Where(s => s.Deleted).Count() == 0);

            // TODO: Проверка БД планирования
        }

        #endregion

        #region Отдельные тесты

        [Fact]
        public void Неизвестный_тип_сообщения_корректно_диагностируется()
        {
            SendStudentMovementMessage("Неизвестный_тип_события");
            string eventId = GetEventId("Неизвестный_тип_события");

            using (var db = GetDbConnection(Db.StudentAdapter))
            {
                RawMessage getMsg() => 
                    db.QuerySingleOrDefault<RawMessage>(
                    $@"
                        SELECT msg.*
                        FROM student_feed_raw_message msg
                        JOIN student_feed_message_log feed ON msg.feed_id = feed.id
                            WHERE msg.message_id = :eventId",
                    new { eventId });

                Feed getFeed() =>
                    db.QuerySingleOrDefault<Feed>(
                    $@"
                        SELECT feed.* 
                        FROM student_feed_raw_message msg
                        JOIN student_feed_message_log feed ON msg.feed_id = feed.id
                            WHERE msg.message_id = :eventId",
                    new { eventId });

                AssertThatEventually(() => getMsg() != null && getFeed() != null);

                var msg = getMsg();
                var feed = getFeed();

                Assert.NotNull(msg);
                Assert.NotNull(feed);

                Assert.Equal("FAILED", feed.ProcessingStatus);
                Assert.True(msg.MessageBody == PrepareTestMessage(GetTestMessage("Неизвестный_тип_события")));
                Assert.True(string.IsNullOrWhiteSpace(feed.ProcessingMessage) || string.IsNullOrWhiteSpace(feed.ProcessingLog));
            }

            AssertWereNoWarnings();
        }

        [Fact]
        public void StudentsControllerApiIsAccesible()
        {
            StudentsControllerApiGet("student/health-check");

            AssertWereNoWarnings();
        }

        [Fact]
        public void Отмена_зачисления_срабатывает_после_подтверждения()
        {
            // Подготовка - стандартное создание двух новых физлиц и студентов
            Assert.False(IsTestPersonExists());

            SendStudentMovementMessage("Новые_физлица");
            SendStudentMovementMessage("Новые_студенты");
            var eventId = GetEventId("Новые_студенты");
            AssertThatEventually(() => AllOperationsHasStatus(eventId, Operation.SUCCESSFUL),
                stateDescription: $"Все операции создания студентов успешно завершены");

            var persons = GetTestPersonsOrdered();
            var students = GetTestStudentsOrdered(persons);

            // Отмена зачисления одного студента
            SendStudentMovementMessage("Отмена_зачисления");
            eventId = GetEventId("Отмена_зачисления");

            AssertThatEventually(() => AllOperationsHasStatus(eventId, Operation.SUSPENDING),
                stateDescription: $"Операция отмены зачисления остановлена");

            // Проверим, что оба студента живы
            Assert.True(GetTestStudentsOrdered(persons).Where(s => s.Deleted).Count() == 0);

            // Подтвердим отмену зачисления
            StudentsControllerApiPost("operation/activate-operation", body: GetOperations(eventId).Select(op => op.Id).ToArray());

            // Изменение должен быть синхронным с получением ответа от REST API!
            Assert.All(GetOperations(eventId), op =>
                {
                    Assert.True(op.StatusId != Operation.CANCELED && op.StatusId != Operation.SUSPENDING, 
                        "Операции отмены зачисления не подтверждены синхронно с вызовом REST API");
                });

            // А в итоге все должно превратиться в успешную отмену зачисления
            AssertThatEventually(() => AllOperationsHasStatus(eventId, Operation.SUCCESSFUL),
                stateDescription: $"Операция отмены зачисления выполнена");

            // Проверка удаленности студентов
            AssertThatEventually(() => GetTestStudentsOrdered(persons).Where(s => s.Deleted).Count() == 1,
                stateDescription: $"После подтверждения отчисления один удален");

            // TODO: Проверить БД планирования

            AssertWereNoWarnings();
        }

        [Fact]
        public void При_создании_пары_студентов_некорректный_не_мешает_корректному()
        {
            const string testMsgName = "Два_новых_студента_с_одним_корректным";

            Создание_пары_физлиц();

            Assert.True(IsTestPersonExists());

            var persons = GetTestPersonsOrdered();

            SendStudentMovementMessage(testMsgName);

            AssertThatEventually(() => GetTestStudentsOrdered(persons).Count == 1,
                stateDescription: $"Есть один тестовый студент");

            var students = GetTestStudentsOrdered(persons);
            Assert.Equal(1, students.Count);

            var student = students.Single();

            // Только второй студент правильный
            var testAttrs = GetTestMessageElementAttributes(testMsgName, $"Событие/Люди/Человек[{2}]/Студент");

            Assert.False(student.Deleted);

            // Проверим маппинги
            AssertPersonOrStudentMappingExistsAndCorrect("student_mapping", testAttrs["Идентификатор"], student.Id);
            AssertFlowMappingExistsAndCorrect(student.CurriculumFlowId, testAttrs["ИдентификаторПотокаОбучения"]);
         
            // Проверка записи события
            var eventId = GetEventId(testMsgName);
            AssertThatEventually(() => IsEventExists(eventId),
                stateDescription: $"Запись события о новом студенте заведена");

            var evt = GetEventWithCorrectnessAssertion(eventId, Event.STUDENT_ENROLLMENT, GetEventDateTime(testMsgName));

            // Проверка успешности операции
            AssertThatEventually(() => AllOperationsHasStatus(eventId, Operation.SUCCESSFUL, Operation.WARNING),
                stateDescription: $"Все операции создания студентов завершены (частично успешны)");

            AssertWereNoWarnings();
        }

        #endregion
    }
}
