using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;
using Serilog;
using Microsoft.Extensions.Configuration;

namespace SmartBroker.Tests.Tests
{
    public class MessageSender
    {
        private ILogger Logger { get; }
        private IConfiguration Configuration { get; }
        public ConnectionFactory Factory { get; set; }

        public MessageSender(IConfiguration configuration, ILogger logger)
        {
            Logger = logger;
            Configuration = configuration;
            Factory = 
                new ConnectionFactory()
                {
                    HostName = GetCfg("Host"),
                    Port = int.Parse(GetCfg("Port")),
                    VirtualHost = GetCfg("VirtualHost"),
                    UserName = GetCfg("UserName"),
                    Password = GetCfg("Password")
                };
        }

        private string GetCfg(string keyName) => Configuration[$"Messaging:{keyName}"];

        public void SendMessage(string messageId, string exchangeName, string routingKey, string body, string contentType, Dictionary<string, string> headers)
        {
            using (var connection = Factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                var props = channel.CreateBasicProperties();
                props.ContentType = contentType;
                props.DeliveryMode = 2;
                props.MessageId = messageId;
                props.Headers = new Dictionary<string, object>(headers.Select(kv => new KeyValuePair<string, object>(kv.Key, kv.Value)));

                var bodyBytes = Encoding.UTF8.GetBytes(body);
                channel.BasicPublish(exchangeName, routingKey, props, bodyBytes);

                Logger.Information("AMQP message sent to {ExchangeName} (r:{RoutingKey})", exchangeName, routingKey);
            }
        }
    }
}