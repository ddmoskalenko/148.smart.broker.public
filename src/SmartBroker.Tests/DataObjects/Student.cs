using System;

namespace SmartBroker.Tests.Tests
{
    public class Student
    {
        public string Id { get; set; }

        public string AudCreatedBy { get; set; }
        public DateTime AudCreatedTs { get; set; }
        public string AudLastUpdatedBy { get; set; }
        public DateTime? AudLastUpdatedTs { get; set; }

        public int Version { get; set; }

        public bool Deleted { get; set; }

        public string PersonId { get; set; }
        public string CurriculumFlowId { get; set; }

        public DateTime LearningStartDate { get; set; }
        public DateTime? LearningEndDate { get; set; }
        public string LearningEndReason { get; set; }

        public string SpecialtyCode { get; set; }
        public string SpecialtyName { get; set; }
        public string SpecialtyProfile { get; set; }
    }
}
