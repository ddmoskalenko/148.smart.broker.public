using System;

namespace SmartBroker.Tests.Tests
{
    public class Feed
    {
        public string Id { get; set; }

        public string AudCreatedBy { get; set; }
        public DateTime? AudCreatedTs { get; set; }
        public string AudLastUpdatedBy { get; set; }
        public DateTime? AudLastUpdatedTs { get; set; }

        public string FeedType { get; set; }
        public DateTime? FeedTimestamp { get; set; }
        public DateTime DeliveryTimestamp { get; set; }
        public DateTime? ProcessingTimestamp { get; set; }

        public string ProcessingStatus { get; set; }
        public string ProcessingErrorCode { get; set; }
        public string ProcessingMessage { get; set; }
        public string ProcessingLog { get; set; }
    }
}
