using System;

namespace SmartBroker.Tests.Tests
{
    public class FlowMapping
    {
        public string Id { get; set; }

        public string AudCreatedBy { get; set; }
        public DateTime? AudCreatedTs { get; set; }
        public string AudLastUpdatedBy { get; set; }
        public DateTime? AudLastUpdatedTs { get; set; }

        public string ExternalId { get; set; }
    }
}
