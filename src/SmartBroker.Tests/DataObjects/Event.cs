using System;

namespace SmartBroker.Tests.Tests
{
    public class Event
    {
        public const string STUDENT_ENROLLMENT = "STUDENT_ENROLLMENT";
        public const string CHANGE_PROFILE = "CHANGE_PROFILE";
        public const string CANCEL_ENROLLMENT = "CANCEL_ENROLLMENT";
        public const string EXPULSION = "STUDENT_EXPULSION";

        public string Id { get; set; }
        public string OperationTypeId { get; set; }
        public DateTime? EventDateTimeUtc { get; set; }
        public string RequestId { get; set; }
    }
}
