using System;

namespace SmartBroker.Tests.Tests
{
    public class Person
    {
        public string Id { get; set; }

        public string AudCreatedBy { get; set; }
        public DateTime AudCreatedTs { get; set; }
        public string AudLastUpdatedBy { get; set; }
        public DateTime? AudLastUpdatedTs { get; set; }

        public int Version { get; set; }

        public string Surname { get; set; }
        public string Name { get; set; }
        public string MiddleName { get; set; }
        public DateTime Birthdate { get; set; }
        public bool IsArchived { get; set; }
        public string Gender { get; set; }
        public string FullName { get; set; }
    }
}
