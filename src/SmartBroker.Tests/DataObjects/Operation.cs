using System;

namespace SmartBroker.Tests.Tests
{
    public class Operation
    {
        public const string SUSPENDING = "SUSPENDING";
        public const string SUCCESSFUL = "SUCCESSFUL";
        public const string CANCELED = "CANCELED";
        public const string WARNING = "WARNING";
        public const string FAILED = "FAILED";

        public string Id { get; set; }

        public string AudCreatedBy { get; set; }
        public DateTime AudCreatedTs { get; set; }
        public string AudLastUpdatedBy { get; set; }
        public DateTime? AudLastUpdatedTs { get; set; }

        public int Version { get; set; }

        public string OperationTypeId { get; set; }
        public string StatusId { get; set; }
        public string StudentId { get; set; }
        public string EventId { get; set; }
        public DateTime ApplyDate { get; set; }
        public DateTime LastUpdateTimestamp { get; set; }
    }
}
