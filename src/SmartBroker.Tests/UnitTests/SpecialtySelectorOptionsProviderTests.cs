﻿using System;
using System.Linq;
using Moq;
using Xunit;


namespace SmartBroker.Tests.Tests.UnitTests
{
    public class SpecialtySelectorOptionsProviderTests

    {
        private readonly SpecialtySelectorOptionsProvider _service;

        private readonly Mock<ICurriculumDescriptionService> _curriculumDescriptionServiceMock;

        public SpecialtySelectorOptionsProviderTests()
        {
            _curriculumDescriptionServiceMock = new Mock<ICurriculumDescriptionService>(MockBehavior.Strict);
            _service = new SpecialtySelectorOptionsProvider(_curriculumDescriptionServiceMock.Object);
        }


        [Fact]
        public async void GetSelectorOptionsAsync_FewSpecialties_ReturnCorrectOptions()
        {
            // Arrange
            var specialties = new[]
            {
                new Speciality { SpecialityCode = "1", SpecialityName = "Специальность 1" },
                new Speciality { SpecialityCode = "2", SpecialityName = "Специальность 2" },
                new Speciality { SpecialityCode = "3", SpecialityName = "Специальность 3" }
            };
            _curriculumDescriptionServiceMock.Setup(x => x.GetSpecialtiesAsync(null)).ReturnsAsync(specialties);

            var expectedOptions = specialties.ToDictionary(
                x => x.SpecialityCode, x => $"{x.SpecialityCode} {x.SpecialityName}");

            // Act
            var result = await _service.GetSelectorOptionsAsync(GetDataQuery());

            var actualOptions = result.Items.ToDictionary(x => x.Key, x => x.Name);

            // Assert
            Assert.Equal(expectedOptions, actualOptions);
        }

        [Fact]
        public async void GetSelectorOptionsAsync_CodePartInPattern_ReturnOptionsWithCodePart()
        {
            const string SearchPattern = "Кусочек кода";
            const string ExpectedCode = SearchPattern + "остальная часть";

            // Arrange
            var specialties = new[]
            {
                new Speciality { SpecialityCode = ExpectedCode, SpecialityName = "Специальность 1" },
                new Speciality { SpecialityCode = "", SpecialityName = "Специальность 2" },
                new Speciality { SpecialityCode = "3", SpecialityName = "Специальность 3" }
            };
            _curriculumDescriptionServiceMock.Setup(x => x.GetSpecialtiesAsync(null)).ReturnsAsync(specialties);

            var dataQuery = new DataQuery<SelectorFilter, SelectorOptionsSorting>
                { Filter = new SelectorFilter { SearchPattern = SearchPattern } };

            // Act
            var result = await _service.GetSelectorOptionsAsync(dataQuery);

            // Assert
            Assert.Equal(ExpectedCode, result.Items.Single().Key);
        }

        [Fact]
        public async void GetSelectorOptionsAsync_NamePartInPattern_ReturnOptionsWithNamePart()
        {
            const string SearchPattern = "Физика";

            // Arrange
            var specialties = new[]
            {
                new Speciality { SpecialityCode = "1", SpecialityName = "Прикладная" + SearchPattern },
                new Speciality { SpecialityCode = "2", SpecialityName = SearchPattern + " Эксперементальная" },
                new Speciality { SpecialityCode = "3", SpecialityName = "Специальность 3" }
            };
            var expectedCodes = new[] { "1", "2" };

            _curriculumDescriptionServiceMock.Setup(x => x.GetSpecialtiesAsync(null)).ReturnsAsync(specialties);

            var dataQuery = new DataQuery<SelectorFilter, SelectorOptionsSorting>
                { Filter = new SelectorFilter { SearchPattern = SearchPattern } };

            // Act
            var result = await _service.GetSelectorOptionsAsync(dataQuery);

            // Assert
            Assert.Equal(expectedCodes, result.Items.Select(x => x.Key));
        }

        [Fact]
        public async void GetSelectorOptionsAsync_FewSpecialtiesWithSameCodes_ReturnCorrectOption()
        {
            // Arrange
            var specialties = new[]
            {
                new Speciality { SpecialityCode = "1", SpecialityName = "Специальность 2" },
                new Speciality { SpecialityCode = "1", SpecialityName = "Специальность 1" },
                new Speciality { SpecialityCode = "3", SpecialityName = "Специальность 3" }
            };

            _curriculumDescriptionServiceMock.Setup(x => x.GetSpecialtiesAsync(null)).ReturnsAsync(specialties);


            // Act &Assert
            var exception =
                await Assert.ThrowsAsync<UserException>(() => _service.GetSelectorOptionsAsync(GetDataQuery()));

            Assert.Equal(
                "Для направления подготовки c \"1\" обнаружено несколько названий: (Специальность 2, Специальность 1)",
                exception.Message);
        }


        private static DataQuery<SelectorFilter, SelectorOptionsSorting> GetDataQuery()
        {
            return new DataQuery<SelectorFilter, SelectorOptionsSorting>();
        }
    }
}