﻿using System;
using System.Linq;
using System.Collections.Generic;
using Moq;
using Xunit;


namespace SmartBroker.Tests.Tests.UnitTests
{
    public class StudentSelectorOptionsProviderTests

    {
        private readonly StudentSelectorOptionsProvider _service;

        private readonly Mock<IStudentRepository> _studentRepositoryMock;

        private readonly Mock<IStudentRemoteRepository> _studentRemoteRepositoryMock;

        public StudentSelectorOptionsProviderTests()
        {
            _studentRepositoryMock = new Mock<IStudentRepository>(MockBehavior.Strict);
            _studentRemoteRepositoryMock = new Mock<IStudentRemoteRepository>(MockBehavior.Strict);
            _service = new StudentSelectorOptionsProvider(
                _studentRemoteRepositoryMock.Object, _studentRepositoryMock.Object);
        }

        [Fact]
        public async void GetSelectorOptionsAsync_EmptyFilter_ReturnInfoAboutAllStudents()
        {
            // Arrange
            var remoteStudents = new[]
            {
                GetStudent("3"),
                GetStudent("1"),
                GetStudent("2")
            };

            _studentRemoteRepositoryMock.Setup(x => x.GetStudentsInfoAsync(null, null))
                .ReturnsAsync(remoteStudents);

            // Act
            var result =
                await _service.GetSelectorOptionsAsync(new DataQuery<StudentSelectorFilter, SelectorOptionsSorting>());

            var actualOptions = result.Items.Select(x => x.Key);

            // Assert
            Assert.Equal(remoteStudents.Select(x => x.Id).OrderBy(x => x), actualOptions);
        }


        [Fact]
        public async void GetSelectorOptionsAsync_FilterWithCodes_FindInfoAboutAllStudentByCodes()
        {
            // Arrange
            var remoteStudents = new StudentRemote[0];

            var filter = new StudentSelectorFilter
            {
                Keys = new[] { "1", "2", "3" }
            };

            _studentRemoteRepositoryMock.Setup(
                    x => x.GetStudentsInfoAsync(null, It.Is<IEnumerable<string>>(i => filter.Keys.SequenceEqual(i))))
                .ReturnsAsync(remoteStudents);

            // Act
            await _service.GetSelectorOptionsAsync(
                new DataQuery<StudentSelectorFilter, SelectorOptionsSorting> { Filter = filter });

            // Assert
            _studentRemoteRepositoryMock.VerifyAll();
        }


        [Fact]
        public async void
            GetSelectorOptionsAsync_FilterWithCodesAndSpecialties_FindStudentsByCodesAndSpecialties()
        {
            // Arrange
            var filter = new StudentSelectorFilter
            {
                Keys = new[] { "1", "2", "3" },
                Specialties = new[] { "Математика", "Физика" }
            };

            var studentsWithDefinedSpecialties = new[]
            {
                new StudentController.DataAccess.Student { StudentId = "1" },
                new StudentController.DataAccess.Student { StudentId = "2" }
            };

            var expectedStudentIds = studentsWithDefinedSpecialties.Select(x => x.StudentId).Intersect(filter.Keys);

            _studentRemoteRepositoryMock.Setup(
                    x => x.GetStudentsInfoAsync(
                        null, It.Is<IEnumerable<string>>(i => expectedStudentIds.SequenceEqual(i))))
                .ReturnsAsync(new StudentRemote[0]);

            _studentRepositoryMock.Setup(x => x.GetStudentsBySpecialties(filter.Specialties))
                .Returns(studentsWithDefinedSpecialties);

            // Act
            await _service.GetSelectorOptionsAsync(
                new DataQuery<StudentSelectorFilter, SelectorOptionsSorting> { Filter = filter });

            // Assert
            _studentRemoteRepositoryMock.VerifyAll();
        }

        [Fact]
        public async void GetSelectorOptionsAsync_FilterWithSpecialties_FindStudentsSpecialties()
        {
            // Arrange
            var filter = new StudentSelectorFilter
            {
                Specialties = new[] { "Математика", "Физика" }
            };

            var studentsWithDefinedSpecialties = new[]
            {
                new StudentController.DataAccess.Student { StudentId = "1" },
                new StudentController.DataAccess.Student { StudentId = "2" }
            };

            var expectedStudentIds = studentsWithDefinedSpecialties.Select(x => x.StudentId);

            _studentRemoteRepositoryMock.Setup(
                    x => x.GetStudentsInfoAsync(
                        null, It.Is<IEnumerable<string>>(i => expectedStudentIds.SequenceEqual(i))))
                .ReturnsAsync(new StudentRemote[0]);

            _studentRepositoryMock.Setup(x => x.GetStudentsBySpecialties(filter.Specialties))
                .Returns(studentsWithDefinedSpecialties);

            // Act
            await _service.GetSelectorOptionsAsync(
                new DataQuery<StudentSelectorFilter, SelectorOptionsSorting> { Filter = filter });

            // Assert
            _studentRemoteRepositoryMock.VerifyAll();
        }

        private static StudentRemote GetStudent(string id)
        {
            return new StudentRemote { Id = id, Person = new PersonRemote { FullName = $"Name {id}" } };
        }
    }
}