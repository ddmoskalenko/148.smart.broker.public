using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Serilog;
using Serilog.Events;
using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace SmartBroker.Tests.Tests
{
    public abstract class IntegrationTestBase
    {
        private const int DEFAULT_WAITING_TIMEOUT_SECONDS = 240;
        private const int DEFAULT_ALMOST_NOW_DELTA_SECONDS = 60;

        protected MessageSender MessageSender { get; }
        protected ILogger Logger { get; }
        protected IConfiguration Configuration { get; }
        protected RemoteRestApiFactory RemoteRestApiFactory { get; }

        protected IntegrationTestBase(ITestOutputHelper output)
        {
            Configuration = new ConfigurationBuilder()
                .AddJsonFile("TestSettings.json")
                .Build();

            Logger = Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Configuration)
                .WriteTo.Debug()
                .WriteTo.Console()
                .WriteTo.TestOutput(output, LogEventLevel.Verbose)
                .CreateLogger()
                .ForContext(GetType());

            Logger.Information($"Test instance created: {GetType().FullName}");

            MessageSender = new MessageSender(Configuration, Logger);

            RemoteRestApiFactory = new RemoteRestApiFactory(
                Configuration["RestApi:ServiceId"],
                GetTestAuthenticationContext(),
                GetTestHttpContextAccessor(),
                new Logger());

            Warnings.Clear();
        }

        private IAuthenticationContext GetTestAuthenticationContext()
        {
            var jwtEncoded = Configuration["RestApi:TestIdToken"];
            var token = (JwtSecurityToken)new JwtSecurityTokenHandler().ReadToken(jwtEncoded);
            var userId = token.Claims.First(claim => claim.Type == "sub").Value;
            var personId = token.Claims.First(claim => claim.Type == "person_id").Value;

            var ctx = new AuthenticationContext();
            ctx.SetAuthenticationData(new ConstantAuthenticationDataSource(new AuthenticationData(userId, personId, jwtEncoded)));
            return ctx;
        }

        private IHttpContextAccessor GetTestHttpContextAccessor()
            => new HttpContextAccessor { HttpContext = null };

        protected static string GetTestMessage(string fileName)
        {
            var asm = typeof(IntegrationTestBase).Assembly;
            var resNames = asm.GetManifestResourceNames();
            var resName = resNames.SingleOrDefault(n => n.Contains(fileName))
                ?? throw new ArgumentException($"Message resource \"...{fileName}...\" not found", nameof(fileName));

            using (var stream = asm.GetManifestResourceStream(resName))
            {
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        protected NpgsqlConnection GetDbConnection(string dbName)
        {
            var csb = new NpgsqlConnectionStringBuilder
            {
                Host = Configuration["Database:Host"],
                Port = int.Parse(Configuration["Database:Port"]),
                Database = dbName,
                Username = Configuration[$"Database:{dbName}:UserName"],
                Password = Configuration[$"Database:{dbName}:Password"],
                Pooling = true
            };

            return new NpgsqlConnection(csb.ToString());
        }

        #region Warning'и в тестах

        protected IList<XunitException> Warnings { get; } = new List<XunitException>();

        protected void WarningAssertThat(bool condition, string errorMessage) 
            => TreatAsWarning(() => Assert.True(condition, errorMessage));

        protected void AssertWereNoWarnings() 
            => Assert.True(Warnings.Count == 0, $"Было {Warnings.Count} предупреждений в процессе тестирования.");

        protected void TreatAsWarning(Action assertAction)
        {
            try
            {
                assertAction();
            }
            catch (XunitException e)
            {
                Warnings.Add(e);
                Logger.Warning($"Warning: {e.Message}");
            }
        }

        #endregion

        #region Дополнительные Assert'ы

        protected void AssertThatEventually(Func<bool> check, TimeSpan timeout = default, string stateDescription = "(?)")
        {
            timeout = (timeout == default) ? TimeSpan.FromSeconds(DEFAULT_WAITING_TIMEOUT_SECONDS) : timeout;

            var startTime = DateTime.Now;
            bool isTimeOut = false;

            Logger.Information("Ждем максимум {SecondsToTimeout} с. состояния \"{StateDescription}\"",
                timeout.TotalSeconds,
                stateDescription);

            while (!check() && !isTimeOut)
            {
                var now = DateTime.Now;
                Thread.Sleep(TimeSpan.FromSeconds(1));
                isTimeOut = (now - startTime) > timeout;
                Logger.Debug("...ждем еще {SecondsToTimeout} с. состояния \"{StateDescription}\"", 
                    (int)(startTime + timeout - now).TotalSeconds,
                    stateDescription);
            }

            if (isTimeOut)
            {
                Logger.Error("Не дождались состояния \"{StateDescription}\"", stateDescription);
                throw new XunitException($"Не дождались состояния \"{stateDescription}\"");
            }
            else
            {
                Logger.Information("Успешно дождались состояния \"{StateDescription}\"", stateDescription);
            }
        }

        protected static void AssertIsAlmostNowUtc(DateTime? dt, int? deltaSec = null, string valueSource = null)
        {
            var now = DateTime.Now.ToUniversalTime();
            var allowedDelta = TimeSpan.FromSeconds(deltaSec ?? DEFAULT_ALMOST_NOW_DELTA_SECONDS);
            TimeSpan? actualDelta = dt == null ? null : (dt > now) ? dt - now : now - dt;

            if (dt == null || actualDelta > allowedDelta)
            {
                string sourceStr = valueSource == null ? "" : $" from {valueSource}";
                throw new XunitException($"DateTime value {dt}{sourceStr} is not almost now UTC ({now})" 
                    + $" (max delta = {allowedDelta}, actual delta = {actualDelta})");
            }
        }

        #endregion
    }
}