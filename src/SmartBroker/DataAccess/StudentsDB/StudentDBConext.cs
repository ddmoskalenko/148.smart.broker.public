using System;
using System.ComponentModel;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using SmartBroker.Api;
using SmartBroker.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using System.Xml.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

using Serilog;
using Npgsql.NameTranslation;


namespace SmartBroker.DataAccess
{
    /// <summary>Контекст рабочей базы сущностей</summary>
    public class StudentDBContext : DbContext
    {
        protected readonly ILogger Logger;
        private readonly IWritableAuthenticationContext AuthentiationContext;
        public IConfiguration Configuration { get; private set; }
       
        /// <summary>Таблица потоков обучения</summary>
        public DbSet<CurriculumFlow> CurriculumFlow { get; set; }
        /// <summary></summary>
        public DbSet<Student> Student { get; set; }
        /// <summary>Список зарегистрированных операций</summary>
        public DbSet<Operation> Operation { get; set; }

        /// <summary>Список выполненных операций по сервисам</summary>
        public DbSet<OperationResult> OperationResult { get; set; }
       
        /// <summary>Отчет по шагам выполненных операций</summary>
        public DbSet<StepResult> StepResult { get; set; }
       
        /// <summary>Входящее событие</summary>
        public DbSet<Event> Event { get; set; }


        private Microsoft.EntityFrameworkCore.Metadata.IMutableModel model;
        private ModelBuilder modelBuilder;
        public StudentDBContext(DbContextOptions<StudentDBContext> options,  IWritableAuthenticationContext authenticationContext, ServiceUserAuthenticator serviceUserAuthenticator, Logger logger) : base(options)
        {
            if(logger!=null) Logger = logger.GetLog<StudentDBContext>();
            AuthentiationContext = authenticationContext;

        }

        /// <summary></summary>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            
            modelBuilder = builder;

            // to lower for postgres
            builder.Model.GetEntityTypes()
                .Select(p => p.Relational())
                .ToList()
                .ForEach(p => p.TableName = NpgsqlSnakeCaseNameTranslator.ConvertToSnakeCase(p.TableName));

            builder.Model.GetEntityTypes()
                .SelectMany(e => e.GetProperties())
                .ToList()
                .ForEach(p => p.Relational().ColumnName = NpgsqlSnakeCaseNameTranslator.ConvertToSnakeCase(p.Name));

            foreach (var property in builder.Model.GetEntityTypes()
                .SelectMany(t => t.GetProperties()))
            {
                if (property.ClrType == typeof(decimal) || property.ClrType == typeof(decimal?))
                    property.Relational().ColumnType = "decimal(18, 4)";
                if (property.ClrType == typeof(DateTime) || property.ClrType == typeof(DateTime?))
                {
                    if (property.PropertyInfo.GetCustomAttributes(false).OfType<DataTypeAttribute>().Any(a => a.DataType == DataType.Date))
                    {
                        property.Relational().ColumnType = "date";
                    }
                }
            }
            // добавляем комментарии
            ModelComment.CreateFromXmlDocFile(builder.Model).AddCommentsToModel(builder);

            model = builder.Model;
            // Soft Delete
            modelBuilder.SetSoftDeleteFilterAll();
        }








        /*
 public async void Rollback()
    {
        var oldBehavoir = ChangeTracker.QueryTrackingBehavior;
        var oldAutoDetect = ChangeTracker.AutoDetectChangesEnabled;

        // this is the key - disable change tracking logic so EF does not check that there were exception in on of tracked entities
        ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        ChangeTracker.AutoDetectChangesEnabled = false;

        var entries = ChangeTracker.Entries().ToList();

        foreach (var entry in entries)
        {
            switch (entry.State)
            {
                case EntityState.Modified:
                    await entry.ReloadAsync();
                    break;
                case EntityState.Deleted:
                    entry.State = EntityState.Modified; //Revert changes made to deleted entity.
                    entry.State = EntityState.Unchanged;
                    break;
                case EntityState.Added:
                    entry.State = EntityState.Detached;
                    break;
            }
        }

        ChangeTracker.QueryTrackingBehavior = oldBehavoir;
        ChangeTracker.AutoDetectChangesEnabled = oldAutoDetect;
        return;
    }
 */
   
 
 /// <summary>Распространяет каскадно удаления по дочерним коллекциям</summary>
        private void ApplyCascadeDeletes()
        {
            var entries = ChangeTracker.Entries().ToList();
            entries.ApplyCascadeDeletes();
        }

        /// <summary>Превращает удаления записей в "логические удаления"</summary>
        private void TransformDeletesToSafe()
        {
            foreach (var entry in ChangeTracker.Entries<BaseAudEntity>())
            {
                if (entry.State == EntityState.Deleted)
                {
                    entry.Entity.Deleted = true;
                    entry.State = EntityState.Modified;
                } 
            }
        }

        /// <summary></summary>
        public override int SaveChanges()
        {
            ApplyCascadeDeletes();
            TransformDeletesToSafe();

            var added = new List<BaseAudEntity>();
            var modified = new List<BaseAudEntity>();

            foreach (var audEntity in ChangeTracker.Entries<BaseAudEntity>())
            {
                if (audEntity.State == EntityState.Added)
                {
                    added.Add(audEntity.Entity);
                }
                else if (audEntity.State == EntityState.Modified)
                {
                    modified.Add(audEntity.Entity);
                }
            }
            foreach (var audEntity in ChangeTracker.Entries<Student>())
            {
                audEntity.Entity.LastUpdateTimestamp = DateTime.Now;
                if (audEntity.State == EntityState.Modified)
                    audEntity.Entity.Version = audEntity.Entity.Version + 1;
            }

            var userId = GetUserId();
            if (string.IsNullOrEmpty(userId))
                throw new SystemException("Ошибка определения текущего пользователя");

            BaseAudEntity.SetAudCreated(added, userId);
            BaseAudEntity.SetAudUpdated(modified, userId);

            return base.SaveChanges();
        }
     
        
        public string GetUserId() =>
            AuthentiationContext.GetAuthenticationData(false)?.UserId;

        private void WorkAroundForSafeDelete()
        {
            var entries = ChangeTracker.Entries().ToList();
            entries.ApplyCascadeDeletes();
        }

        private void OnBeforeSavingForSafeDelete()
        {
        }
    }
}
