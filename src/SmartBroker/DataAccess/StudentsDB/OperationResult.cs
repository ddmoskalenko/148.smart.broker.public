using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SmartBroker.DataAccess
{

    /// <summary>Результат операции в сервисах</summary>
    public class OperationResult : BaseEntity
    {
        /// <summary>Идентификатор операции</summary>
        [Required]
        [MaxLength(100)]
        public string OperationId { get; set; }
        public Operation Operation { get; set; }
        
        /// <summary>Отметка об успешности результата операции</summary>
        public bool Success { get; set; }

        /// <summary>Шаги входящие в результат операции</summary>ot
        public virtual List<StepResult> StepResults { get; set; }

        /// <summary>Сервис, в котором выполнялась эта операция</summary>
        [Required]
        [MaxLength(100)]
        public string ServiceAgentId { get; set; }
        public ServiceAgentType ServiceAgent { get; set; }

        public OperationResult()
        {    
            
        
        }

        /// <summary>Нумерация для сортировки</summary>
        public void Numerate()
        {
            for(int i=0; i<StepResults.Count; i++)
            {
                StepResults[i].OrderIndex = i; 
            }
        }
    }
}