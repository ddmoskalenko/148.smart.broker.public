using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SmartBroker.DataAccess
{
    /// <summary>Статус операции/шага по движению сущность</summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum OperationStatus
    {
        /// <summary>Выполнено успешно</summary>
        [EnumMember(Value = "SUCCESSFUL")]
        SUCCESSFUL,

        /// <summary>Техническая проблема</summary>
        [EnumMember(Value = "EXCEPTION")]
        EXCEPTION,

        /// <summary>Некорректные данные</summary>
        [EnumMember(Value = "INVALID_DATA")]
        INVALID_DATA,

        /// <summary>Бизнес-ограничения</summary>
        [EnumMember(Value = "BUSINESS_RULE_VIOLATED")]
        BUSINESS_RULE_VIOLATED,

        /// <summary>Ожидается выполнение</summary>
        [EnumMember(Value = "AWAITING")]
        AWAITING,

        /// <summary>Выполнено с ограничениями</summary>
        [EnumMember(Value = "WARNING")]
        WARNING,
        
        /// <summary>Приостановлен</summary>
        [EnumMember(Value = "SUSPENDING")]
        SUSPENDING,

        /// <summary>Обрабатывается</summary>
        [EnumMember(Value = "PROCESSING")]
        PROCESSING,
 
        /// <summary>Отмененные</summary>
        [EnumMember(Value = "CANCELED")]
        CANCELED,


    }
}
