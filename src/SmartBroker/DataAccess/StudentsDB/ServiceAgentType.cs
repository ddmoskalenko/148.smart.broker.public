using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartBroker.DataAccess
{
    /// <summary>Имя сервиса</summary>
    public class ServiceAgentType
    {
        /// <summary>Сервис A</summary>
        public const string PeriodPlanningService = "periodplanning";

        /// <summary>Сервис B</summary>
        public const string ActualLearingService = "actual-learning-path";

        /// <summary>Сервис C</summary>
        public const string PersonalInfoService = "person-information-service";
 
        /// <summary>Сервис D</summary>
        public const string StudentControllerService = "alumnos-contoller-service";
        
        /// <summary>
        /// Краткое имя сервиса
        /// </summary>
        [Key]
        [MaxLength(100)]
        public string Code { get; set; }
        
        /// <summary>
        /// Полное имя сервиса
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }  

        /// <summary>
        /// Статус работоспособности сервиса
        /// </summary>
        [Required]
        public bool IsActive { get; set; } 

     }
}

