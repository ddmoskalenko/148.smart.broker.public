using System;
using System.ComponentModel.DataAnnotations;


namespace SmartBroker.DataAccess
{
    /// <summary>Сущность</summary>
    public class Student : BaseAudEntity
    {
        /// <summary>Идентификатор сущность</summary>
        [Required]
		public string StudentId {get;set;}

        [Required]
        public int Version {get;set;}

        [Required]
    	public string PersonId {get;set;}

        [Required]
      	public string CurriculumFlowId {get;set;}
        
        [Required]
       	public DateTime LearningStartDate {get;set;}
          
      	public DateTime? LearningEndDate {get;set;}

        public string LearningEndReason {get;set;}
              
        /// <summary></summary>
        public string SpecialtyCode {get;set;}

        /// <summary></summary>
        public string SpecialtyName {get;set;}

        /// <summary></summary>
        public string SpecialtyProfile {get;set;}

    }
}

