using System;
using System.ComponentModel.DataAnnotations;


namespace SmartBroker.DataAccess
{
    /// <summary>
    /// Шаг операции по движению сущность
    /// </summary>
    public class StepResult : BaseEntity
    {
        /// <summary>Имя шага</summary>
        [Required]
        public string StepName  {get;set;}

        /// <summary>Состояние шага операции</summary>
        [Required]
        [MaxLength(200)]
        public string StepStatus { get; set; }

        /// <summary>Детальное описание шага операции</summary>
   		public string Description {get;set;}

        /// <summary>Идентификатор операции, в которую входит этот шаг</summary>
        [Required]
        [MaxLength(100)]
   		public string OperationResultId {get;set;}

        [Required]
        public int OrderIndex  {get;set;}
 
        public OperationResult OperationResult {get;set;}
        public StepResult(StepResult original)
        {
            StepName    = original.StepName;
            StepStatus  = original.StepStatus;
            Description = original.Description;
        }

        public StepResult()
        {        
            
        }
    }
}