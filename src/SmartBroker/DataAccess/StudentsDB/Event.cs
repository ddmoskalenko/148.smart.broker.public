using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

using SmartBroker.Services;

using SmartBroker.Model;

using Serilog;

namespace SmartBroker.DataAccess
{
    /// <summary>Входящие сообщения, получаемые от Student Driver</summary>
    public class Event //: BaseAudEntity
    {
   
        /// <summary> Идентификатор в заголовке XML</summary>
        [Required]
        [MaxLength(100)]
        public string Id { get; set; }

        /// <summary> Идентификатор X-Request-ID входящего HTTP запроса </summary>
        [Required]
        [MaxLength(100)]
        public string RequestId { get; set; }

        /// <summary> Тип операции</summary>
        [Required]
        public string OperationTypeId { get; set; }

        ///<summary> Временная метка из заголовка событий в UTC</summary>
        public DateTime? EventDateTimeUtc { get; set; }

        public static Event CreateEvent(StudentDBContext dbContext, OperationType operationType, StudentArrow arrow, ILogger logger)
        {
            DateTime? eventDateTime = null;
            string dateFormat = CommonSettings.dateformatT;
            try
            {
                eventDateTime =  DateTime.ParseExact(arrow.EventDateTime, dateFormat, new System.Globalization.CultureInfo("en-US"));    
            }
            catch 
            {  
                if (logger!=null) logger.Warning("Переданная дата " + arrow.EventDateTime + " не сооответствует шаблону '"+dateFormat+"'");
            }

            if (dbContext.Event.Where(e=>e.Id == arrow.EventID).Any()) 
            { 
                if (logger!=null) logger.Warning("Дублирование идентификатора события ID ["+arrow.EventID+"]");
                arrow.EventID = Guid.NewGuid().ToString();
            }
            
            var mevent = new SmartBroker.DataAccess.Event() { Id = arrow.EventID, OperationTypeId = operationType.ToString(), RequestId = arrow.RequestID, EventDateTimeUtc = eventDateTime };
            dbContext.Event.Add(mevent);
            return mevent;
        }
    }
}