using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using SmartBroker.Services;

using SmartBroker.Model;

using Serilog;

namespace SmartBroker.DataAccess
{

    /// <summary>Операция по сущностьу в сервисах</summary>
    public class Operation : BaseAudEntity
    {
        /// <summary>Тип операции</summary>
        [Required]
        [MaxLength(100)]
        public string OperationTypeId { get; set; }
    
        /// <summary>Идентификатор события в которое входит эта операция</summary>
        [Required]
        public string EventId { get; set; }
        public Event Event { get; set; }

        /// <summary>Идентификатор состояние операции</summary>
        [Required]
        [MaxLength(100)]
        public string StatusId { get; set; }

        /// <summary>Предыдущий поток обучения</summary>
        [MaxLength(100)]
        public string PreviousCurriculumFlowId { get; set; }

        /// <summary>Предыдущяя специализация</summary>
        [MaxLength(100)]
        public string PreviousSpecialityCode { get; set; }

        /// <summary>Предыдущий профиль обучения</summary>
        public string PreviousProfileName { get; set; }

        /// <summary>Поток обучения</summary>
        [MaxLength(100)]
        public string CurriculumFlowId { get; set; }

        /// <summary>Код направления</summary>
        [MaxLength(100)]
        public string SpecialityCode { get; set; }

        /// <summary>Наименование направления</summary>
        public string SpecialityName { get; set; }

        /// <summary>Профиль обучения</summary>
        public string ProfileName { get; set; }

        /// <summary>Идентификатор сущность</summary>
        [Required]
        [MaxLength(100)]
        public string StudentId { get; set; }

        /// <summary>Сущность</summary>
        public Student Student { get; set; }

        /// <summary>Предыдущий идентификатор сущность</summary>
        [MaxLength(100)]
        public string PreviousStudentID { get; set; }


        /// <summary>Аттрибуты приказа в образовательном учереждении</summary>
        [MaxLength(2000)]
        public string Decree { get; set; }

        /// <summary>Описание причины события</summary>
        [MaxLength(2000)]
        public string Reason { get; set; }

        /// <summary>День с которого событие начинает действовать</summary>
        [Required]
        public DateTime ApplyDate { get; set; }
        public Operation()
        { 

        }

        public Operation(Student alumno)
        {
            StudentId = alumno.StudentId;
            PreviousCurriculumFlowId = alumno.CurriculumFlowId;
            PreviousSpecialityCode =  alumno.SpecialtyCode;
            PreviousProfileName = alumno.SpecialtyProfile;
        }

     
        public Operation(StudentEntity entity, ILogger logger = null)
        {
            CurriculumFlowId = entity.FlowID;
            SpecialityCode = entity.SpecialtyCode;
            SpecialityName = entity.Specialty;
            ProfileName = entity.Profile;

            Decree = entity.Ordnung;
            Reason = entity.RegistryReason;

            ApplyDate = DateTime.Now;
            try
            {
                ApplyDate = DateTime.ParseExact( entity.ApplyDate, CommonSettings.dateformat, new System.Globalization.CultureInfo("en-US"));
            }
            catch
            {
                if (logger!=null) logger.Warning("Дата применения у события для сущность ["+entity.ID+"] не определена, и будет установлена текущая");
            }

            StudentId = entity.ID;
        }

        /// <summary> Переносит в раздел данных о прошлом состоянии программы обучения текущее состояние из базы данных </summary>
        public void SetPrevious(Student alumno)
        {
            PreviousCurriculumFlowId = alumno.CurriculumFlowId;
            PreviousSpecialityCode = alumno.SpecialtyCode;
            PreviousProfileName = alumno.SpecialtyProfile;
            PreviousStudentID =  alumno.StudentId;
        }

        /// <summary> Создает и пишет в базу для списка операции с передаными шагами и статусом</summary>
        public static void CreateOperationsWithResult(StudentDBContext dbContext, OperationType operationType, List<Operation> protoOperations, OperationStatus operationStatus, string eventID, string agentType, bool success, StepResult step = null)
        {
                
            // var operations = Operation.CreateOperation(dbContext, OperationType.CHANGE_PROFILE, protoOperations, OperationStatus.WARNING, eventID);
            var operations = Operation.CreateOperation(dbContext, OperationType.CHANGE_PROFILE, protoOperations, operationStatus, eventID);
            
            foreach(var operation in operations)
            {
                OperationResult operationRes =  new OperationResult(){ OperationId = operation.Id, ServiceAgentId = agentType, Success = success };
                dbContext.OperationResult.Add(operationRes);
                if (step!=null)
                   dbContext.StepResult.Add(new StepResult(step){ OperationResultId = operationRes.Id });
            }
        }

        /// <summary> Создает результат операции с одним шагом для списка операция (обычно для отметок об ошибоке валидации)</summary>        
        public static void CreateResultWithStep(StudentDBContext dbContext, List<Operation> operations, OperationStatus operationStatus, string agentType, bool success, StepResult step = null)
        {
                
            foreach(var operation in operations)
            {
                OperationResult operationRes =  new OperationResult(){ OperationId=operation.Id, ServiceAgentId = agentType, Success = success };
                dbContext.OperationResult.Add(operationRes);
                if(step!=null) dbContext.StepResult.Add(new StepResult(step){ OperationResultId= operationRes.Id});
            }
        }

        /// <summary> Создает список операций и записывает в базу данных </summary>
        public static List<Operation> CreateOperation(StudentDBContext dbContext, OperationType operationType, List<Operation> operations, OperationStatus operationStatus, string eventID, ILogger logger = null)
        {
            operations.ForEach(a => { a.EventId = eventID; a.StatusId = operationStatus.ToString(); a.OperationTypeId = operationType.ToString(); });

            // check that operations at these alumnos are not suspended
            if (operationType == OperationType.CANCEL_ENROLLMENT)
            {// исключаем дублирование активных отмен зачисления
             // если для студета есть не заверешенные отмены - эту считаем сразу отменными
                if (logger!=null) logger.Information("Создаем операцию Отмена");
                 
                var activeCancelEnrollmentStudents = dbContext.Operation.Where(o => operations.Select(s => s.StudentId).Contains(o.StudentId) 
                    && o.OperationTypeId == OperationType.CANCEL_ENROLLMENT.ToString() 
                    && ( o.StatusId == OperationStatus.SUSPENDING.ToString() || o.StatusId == OperationStatus.AWAITING.ToString())).Select(s=>s.StudentId);
                if (activeCancelEnrollmentStudents.Any())
                {
                    if (logger!=null) logger.Information("Уже существуют действующие ОтменыЗачисления, эта Отмена будет игнорироваться");
                    operations.Where(o=> activeCancelEnrollmentStudents.Contains(o.StudentId)).ToList().ForEach(a => { a.StatusId = OperationStatus.CANCELED.ToString(); });
                }
            }
            else
            {// если для сущностей есть остановленные операции, останавливаем и новые
                var alreadySupendedStudents = dbContext.Operation.Where(o => o.StatusId == OperationStatus.SUSPENDING.ToString() && operations.Select(s => s.StudentId).Contains(o.StudentId)).Select(s => s.StudentId).Distinct().ToList();
                if (alreadySupendedStudents.Any())
                {
                    if (logger!=null) logger.Information("Операции по сущностьм ["+string.Join(",",alreadySupendedStudents)+"] приостановлены до решения об ОтменеЗачисления");
                    operations.Where(o=> alreadySupendedStudents.Contains(o.StudentId)).ToList().ForEach(a => { a.StatusId = OperationStatus.SUSPENDING.ToString(); });
                }
            }
            dbContext.Operation.AddRange(operations);
            return operations;
        }

        /// <summary> Создаем результат операции с одинм шагом</summary>
        public static OperationResult CreateOperationResultWithOneStep(string message, Operation operation, OperationStatus operationStatus)
        {
            const string stepNameValidation = "Проверка данных";

            operation.StatusId = operationStatus.ToString();
            OperationResult operationResult = new OperationResult() { OperationId = operation.Id, ServiceAgentId = ServiceAgentType.StudentControllerService, Success = (operationStatus == OperationStatus.SUCCESSFUL ? true : false) };
            StepResult stepResult = new StepResult() { StepName = stepNameValidation, StepStatus = operationStatus.ToString(), Description = message, OperationResultId = operationResult.Id };
            operationResult.StepResults = new List<StepResult> { stepResult };
            return operationResult;
        }



        /// <summary> Для всех указанных сущностей из операций приостанавливает операции, ожидающие обработку </summary>
        public static void SuspendForStudent(StudentDBContext dbContext, List<Operation> operations)
        {
            var forSuspend = dbContext.Operation.Where(o=> o.StatusId == OperationStatus.AWAITING.ToString() && operations.Select(t=>t.StudentId).Contains(o.StudentId)).ToList();
            forSuspend.ForEach(a => { a.StatusId = OperationStatus.SUSPENDING.ToString(); });
        }
    }
}