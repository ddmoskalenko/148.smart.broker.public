using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SmartBroker.DataAccess
{
    /// <summary>Статус операции/шага по движению сущность</summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum OperationType
    {
        /// <summary>Выполнено успешно</summary>
        [EnumMember(Value = "ALUMNO_ENROLLMENT")]
        ALUMNO_ENROLLMENT,

        /// <summary>Смена профиля</summary>
        [EnumMember(Value = "CHANGE_PROFILE")]
        CHANGE_PROFILE,


        /// <summary>Отчисление сущность</summary>
        [EnumMember(Value = "ALUMNO_EXPULTION")]
        ALUMNO_EXPULTION,

        /// <summary>Удаление сущность</summary>
        [EnumMember(Value = "CANCEL_ENROLLMENT")]
        CANCEL_ENROLLMENT,
    }
}
