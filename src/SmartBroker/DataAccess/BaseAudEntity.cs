using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SmartBroker.DataAccess
{
    /// <summary>Базовая сущность с параметрами аудита</summary>
    public abstract class BaseAudEntity : BaseEntity
    {
        /// <summary>
        /// Дата создания записи
        /// </summary>
        [Required]
        public DateTime AudCreatedTs { get; set; }
        
        /// <summary>
        /// Пользователь, создавший запись
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string AudCreatedBy { get; set; }
        
        /// <summary>
        /// Дата изменения записи
        /// </summary>
        public DateTime? AudLastUpdatedTs { get; set; }
        
        /// <summary>
        /// Пользователь, изменивший запись
        /// </summary>
        [MaxLength(100)]
        public string AudLastUpdatedBy { get; set; }


        /// <summary> Дата последнего изменения </summary>
        public DateTime LastUpdateTimestamp { get; set; }

        /// <summary>
        /// Признак удаленного объекта
        /// </summary>
        [Required]
        public bool Deleted {get;set;}

        /// <summary>Массовая установка данных, может использоваться и вне DbContext, например, для NpgsqlBulkUploader</summary>
        public static void SetAudCreated<T>(IEnumerable<T> entityList, string userId)
            where T : BaseAudEntity
        {
            var now = DateTime.Now;
            foreach (var entity in entityList)
            {
                entity.AudCreatedTs = now;
                entity.AudCreatedBy = userId;
                entity.LastUpdateTimestamp = now;
            }
        }

        /// <summary>Массовая установка данных, может использоваться и вне DbContext, например, для NpgsqlBulkUploader</summary>
        public static void SetAudUpdated<T>(IEnumerable<T> entityList, string userId)
            where T : BaseAudEntity
        {
            var now = DateTime.Now;
            foreach (var entity in entityList)
            {
                entity.AudLastUpdatedTs = now;
                entity.AudLastUpdatedBy = userId;
                entity.LastUpdateTimestamp = now;
            }
        }
    }
}