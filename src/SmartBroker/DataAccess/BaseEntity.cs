using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartBroker.DataAccess
{
    /// <summary>Базовая сущность с идентификатором</summary>
    public abstract class BaseEntity
    {
        /// <summary>
        /// Id
        /// </summary>
        [Key]
        [MaxLength(100)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Id { get; set; }

        /// <summary></summary>
        protected BaseEntity()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}