using System;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace SmartBroker.Api
{
    public class GitVersion
    {
        public string Repository;
        public string Commit;
        public string Tag;
        public string Branch;


    }
}