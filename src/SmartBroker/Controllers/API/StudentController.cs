using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Microsoft.AspNet.OData.Query;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using Newtonsoft.Json.Linq;

using SmartBroker.Model;
using SmartBroker.Services;

using Serilog;



namespace SmartBroker.Controllers
{
    /// <summary>
    /// API для адаптера(ов) 1С  
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = false)]
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : BaseSecurityController
    {//         protected StudentDBService StudentDBService = null;
        private readonly Random _random = new Random();
        private static string DBError = "База данных недоступна"; 

        /// <summary>
        /// Конструктор
        /// </summary>
        public StudentController(
                       StudentDBService alumnoDBService,
                       Logger logger, 
                       // ServiceUserAuthenticator serviceUserAuthenticator,
                       IHttpContextAccessor httpContextAccessor,
                       IAuthorizationService authService
                       ) : base(logger, httpContextAccessor, authService)
        {        
            StudentDBService = alumnoDBService;
        }

        /// <summary> работоспособность сервиса </summary>
        [HttpGet("health-check")]
        public IActionResult HealthCheck()
        {
            return Ok(true);
        }
       
        private string GetRequestId()
        {
            return HttpContextAccessor?.HttpContext.GetRequestId() ?? _random.GetBase62String(12);
        }

        /// <summary> новый сущность </summary>
        [HttpPost("alumno-enrollment")]
        public ActionResult<EventResult> StudentEnrollment(StudentArrow arrow)
        {
            Logger.Information("Получен запрос 'НовыйСущность' c идентификатором ["+arrow.EventID+"]");
            arrow.RequestID = GetRequestId();
            EventResult result = null;//new EventResult();
            result = StudentDBService.StudentEnrollment(arrow);
            if (result.ProcessingStatus!=ProcessingStatus.Completed)
            {// единственный сценарий отказа - невозможность сохранить весь список сущностей в таблицу
                return BadRequest(result);
            }
    
            // этот метода сам пишет в историю ответы и отдельный поток повторяет попытки выполнить бизнес-транзакцию        
            string msg = "Операция 'НовыйCтудент' для "+arrow.Students.Count().ToString()+" сущностей ["+string.Join(",",arrow.Students.Select(s=>s.ID))+"] поставлена в очередь";    
            Logger.Information(msg);
            result.ProcessingLog = msg;
            return Ok(result);
        }

        /// <summary> смена профиля  </summary>
        [HttpPost("changing-profile")]
        public ActionResult<EventResult> ChangingProfile(StudentArrow alumnos)
        {
            alumnos.RequestID = GetRequestId();
            try
            {
                EventResult result = StudentDBService.ChangingProfile(alumnos);
                if (result.ProcessingStatus == ProcessingStatus.Failed)
                {
                    Logger.Error(result.ProcessingLog);
                    return result;
                }

                string msg = "Операции 'XXX' для ("+result.InQueue+") сущностей были поставлены в очередь";
                result.ProcessingLog = msg;
                Logger.Information(msg);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, DBError);
                return BadRequest(new EventResult(ex.Message));
            }
        }

        
        /// <summary> отмена</summary>
        [HttpPost("cancel-enrollment")]
        public ActionResult<EventResult> CancelEnrollment(StudentArrow alumnos)
        {
            Logger.Debug("Получен запрос на отмену");
            alumnos.RequestID = GetRequestId();
            try
            {
                EventResult result = StudentDBService.CancelEnrollment(alumnos);
                if (result.ProcessingStatus ==  ProcessingStatus.Failed)
                {
                    Logger.Error(result.ProcessingLog);
                    return BadRequest(result);
                }

                string msg = "Операции 'Отмена' для ("+alumnos.Students.Length+") сущностей поставлены в очередь";
                result.ProcessingLog = msg;
                Logger.Information(msg);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Logger.Error(ex,DBError);
                return BadRequest(new EventResult(ex.Message));
            }
        }
        
        /// <summary> отчисление сущность </summary>
        [HttpPost("alumno-expultion")]
        public IActionResult StudentExpultion(StudentArrow alumnos)
        {
            Logger.Debug("Получен запрос на отчисление сущность");
            alumnos.RequestID = GetRequestId();
            try
            {
                EventResult result = StudentDBService.StudentExpulsion(alumnos);
                if (result.ProcessingStatus == ProcessingStatus.Failed )
                {
                    Logger.Error(result.ProcessingLog);
                    return BadRequest(result);
                }

                string msg = "Операция ["+ alumnos.EventID+"] Отчисление для ("+alumnos.Students.Length+") сущностей поставлена в очередь";
                Logger.Information(msg);
                result.ProcessingLog = msg;
                return Ok(result);
            }
            catch (Exception ex)
            {
                Logger.Error(ex,DBError);
                return BadRequest(new EventResult(ex.Message));
            }
        }
    }
}