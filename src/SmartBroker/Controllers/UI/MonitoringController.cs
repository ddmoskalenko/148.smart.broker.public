using System;
using System.Linq;
using System.Diagnostics; 
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

using SmartBroker.Api;
using SmartBroker.DataAccess;


using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Query;
using Microsoft.AspNet.OData.Routing;
//using Microsoft.AspNetCore.Authorization;
//

using Serilog;

namespace SmartBroker.Controllers
{

    public class MonitoringController : BaseSecurityController
    {
        //protected readonly ConsolidatedPlanService summaryService;
        protected StudentDBContext StudentDBContext = null;

        /// <summary>
        /// dashboard событий контроллера сущностей 
        /// </summary>
        public MonitoringController(StudentDBContext alumnoDbContext, 
            Logger logger,
            IHttpContextAccessor httpContextAccessor, 
            IAuthorizationService authService)
            : base(logger, httpContextAccessor, authService)
        {
            StudentDBContext = alumnoDbContext;
        }
       
        [HttpGet("Monitoring")]
        public ActionResult<IEnumerable<Student>> GetConsolidatePlan(ODataQueryOptions<Student> queryOptions)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var courses = ODataQueryHelper.GetListFromQuery<Student>(StudentDBContext.Student.IgnoreQueryFilters()
                //.Include(e => e.State)
                .AsNoTracking().AsQueryable(),
                queryOptions);

            watch.Stop();
            //Console.WriteLine("******");
            //Console.WriteLine(" ODataQuery courses takes " + watch.ElapsedMilliseconds.ToString());

            //var value = summaryService.CoursesLoading(courses);
            //var data = new { value = value, count = value.Length };
            return Ok(courses);
        }

    }
   
}