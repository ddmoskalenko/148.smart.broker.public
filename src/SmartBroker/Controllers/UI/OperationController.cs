using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Microsoft.AspNet.OData.Query;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using Newtonsoft.Json.Linq;

using SmartBroker.Model;
using SmartBroker.Services;

using Serilog;



namespace SmartBroker.Controllers
{
    /// <summary>
    /// API управления состоянием операций  
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = false)]
    [Route("api/[controller]")]
    [ApiController]
    public class OperationController : BaseSecurityController
    {// 
        protected StudentDBService StudentDBService = null;
        private readonly Random _random = new Random();
        /// <summary>
        /// Конструктор
        /// </summary>
        public OperationController(
                       StudentDBService alumnoDBService,
                       Logger logger, 
                       ServiceUserAuthenticator serviceUserAuthenticator,
                       IHttpContextAccessor httpContextAccessor,
                       IAuthorizationService authService
                       ) : base(logger, httpContextAccessor, authService)
        {        
            StudentDBService = alumnoDBService;
        }
       
        private string GetRequestId()
        {
            return HttpContextAccessor?.HttpContext.GetRequestId() ?? _random.GetBase62String(12);
        }

        /// <summary> отмена списка операций</summary>
        [HttpPost("canceling-operation")]
        public IActionResult CancelOperation([FromBody]string[] operations)
        {
            // string requestID = GetRequestId();
            string reply = StudentDBService.CancelOperation(operations);
            if (!string.IsNullOrEmpty(reply))
            {   
                Logger.Error(reply);
                return BadRequest(reply);
            }
    
            Logger.Information("Запрос на отмену операций ["+string.Join(",", operations)+"] обработан ");
            return Ok(true);
        }

        /// <summary> запустить операцию</summary>
        [HttpPost("activate-operation")]
        public IActionResult ActivateOperation([FromBody]string[] operations)
        {
            string reply = StudentDBService.ActivateOperation(operations);
            if (!string.IsNullOrEmpty(reply))
            {   
                Logger.Error(reply);
                return BadRequest(reply);
            }
    
            Logger.Information("Запрос на запуск операций ["+string.Join(",", operations) +"] обработан ");
            return Ok(true);
        }
    }
}