using System;
using System.Collections.Generic;

namespace SmartBroker.Controllers
{
    /// <summary>
    /// Пользовательский дозапрос
    /// </summary>
    public class UserConfirmation : Exception
    {
        /// <summary>
        /// Пользовательский дозапрос
        /// </summary>
        public UserConfirmation(string message, Dictionary<string,string> options = null)
            :base(message)
        {
            Options = options;
        }

        public Dictionary<string,string> Options {get;private set;}

        public static Dictionary<string, string> DefaultOptions = new Dictionary<string, string>{{"yes","Да"}};
    }
}