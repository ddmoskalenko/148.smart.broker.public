using System;

namespace SmartBroker.Controllers
{
    /// <summary>
    /// Пользовательская ошибка 
    /// </summary>
    public class UserException : Exception
    {
        /// <summary>
        /// Пользовательская ошибка 
        /// </summary>
        public UserException(string message)
            :base(message)
        {
        }

        public static void CheckRequired(object value, string name)
        {
            if (value == null) throw new UserException($"Не задан обязательный параметр [{name}]");
        }
    }
}