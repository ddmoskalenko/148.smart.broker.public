using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;

namespace SmartBroker.Extensions
{
    // https://comment-it.co.uk/entity-framework-core-soft-delete-workaround/
    public static class CascadeDeleteWorkaround
    {
        public static InternalEntityEntry GetInternalEntityEntry(this EntityEntry entityEntry)
        {
            var internalEntry = (InternalEntityEntry)entityEntry
                .GetType()
                .GetProperty("InternalEntry", BindingFlags.NonPublic | BindingFlags.Instance)
                .GetValue(entityEntry);

            return internalEntry;
        }

        public static void ApplyCascadeDeletes(this IEnumerable<EntityEntry> entities)
        {
            foreach (var entry in entities.Where(
               e => (e.State == EntityState.Modified
               || e.State == EntityState.Added)
               && e.GetInternalEntityEntry().HasConceptualNull).ToList())
            {
                entry.GetInternalEntityEntry().HandleConceptualNulls(false);
            }

            foreach (var entry in entities.Where(e => e.State == EntityState.Deleted).ToList())
            {
                CascadeDelete(entry.GetInternalEntityEntry());
            }
        }

        private static void CascadeDelete(InternalEntityEntry entry)
        {
            foreach (var fk in entry.EntityType.GetReferencingForeignKeys())
            {
                foreach (var dependent in (entry.StateManager.GetDependentsFromNavigation(entry, fk)
                         ?? entry.StateManager.GetDependents(entry, fk)).ToList())
                {
                    if (dependent.EntityState != EntityState.Deleted
                        && dependent.EntityState != EntityState.Detached)
                    {
                        if (fk.DeleteBehavior == DeleteBehavior.Cascade)
                        {
                            var cascadeState = dependent.EntityState == EntityState.Added
                               ? EntityState.Detached
                               : EntityState.Deleted;

                            dependent.SetEntityState(cascadeState);

                            CascadeDelete(dependent);
                        }
                        else if (fk.DeleteBehavior != DeleteBehavior.Restrict)
                        {
                            foreach (var dependentProperty in fk.Properties)
                            {
                                dependent[dependentProperty] = null;
                            }

                            if (dependent.HasConceptualNull)
                            {
                                dependent.HandleConceptualNulls(false);
                            }
                        }
                    }
                }
            }
        }
    }
}