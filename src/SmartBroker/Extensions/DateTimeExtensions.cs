using System;

namespace SmartBroker.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek = DayOfWeek.Monday)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }

        public static DateTime EndOfWeek(this DateTime dt, DayOfWeek startOfWeek = DayOfWeek.Monday)
        {
            return StartOfWeek(dt, startOfWeek).AddDays(6);
        }

        public static DateTime StartOfMonth(this DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, 1);
        }

        public static DateTime EndOfMonth(this DateTime dt)
        {
            return StartOfMonth(dt).AddMonths(1).AddDays(-1);
        }

        public static string ToDefaultString(this DateTime dt)
        {
            return dt == null ? null : dt.ToString("dd.MM.yyyy");
        }

        public static string ToShortYearString(this DateTime dt)
        {
            return dt == null ? null : dt.ToString("dd.MM.yy");
        }

        public static int GetAcademicYearFrom(this DateTime dt)
        {
            return (dt.Month <= 8) ? dt.Year - 1 : dt.Year;
        }
    }
}