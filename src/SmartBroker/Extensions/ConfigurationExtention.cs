using Microsoft.Extensions.Configuration;
using System;

namespace SmartBroker.Extensions
{
    /// <summary>
    /// Методы расширения к <see cref="IConfiguration"/>
    /// </summary>
    public static class ConfigurationExtensions
    {
        /// <summary>
        /// Преобразует текущую секцию конфигурации в <see cref="DatabaseConnectionOptions"/>
        /// </summary>
        public static DatabaseConnectionOptions ToDatabaseConnectionOptions(this IConfiguration configuration, string databasePrefix)
        {
            if (configuration is null) throw new ArgumentNullException(nameof(configuration));
            if (databasePrefix is null) throw new ArgumentNullException(nameof(databasePrefix));

            return new DatabaseConnectionOptions
            {
                Url = configuration.GetNotNullString($"{databasePrefix}.url"),
                User = configuration.GetNotNullString($"{databasePrefix}.username"),
                Password = configuration.GetNotNullString($"{databasePrefix}.password"),
                AdditionalOptions = configuration.GetStringOrDefault($"{databasePrefix}.options", null)
            };
        }

        /// <summary>
        /// Преобразует текущую секцию конфигурации в <see cref="RestApiConnectionOptions"/>
        /// </summary>
        public static RestApiConnectionOptions ToRestApiConnectionOptions(this IConfiguration configuration, string restApiKey)
        {
            if (configuration is null) throw new ArgumentNullException(nameof(configuration));
            if (restApiKey is null) throw new ArgumentNullException(nameof(restApiKey));

            return new RestApiConnectionOptions
            {
                Url = configuration.GetNotNullString(restApiKey)
            };
        }
    }
}
