using System;
using System.Linq;

namespace SmartBroker.Extensions
{
    /// <summary></summary>
    public static class DatabaseConnectionUrlParser
    {
        /// <summary></summary>
        public static string Parse(string urlString)
        {
            var url = new Uri(urlString.Substring(5), UriKind.Absolute);
            var queryDictionary = System.Web.HttpUtility.ParseQueryString(url.Query);
            var connectionString = $"Host={url.Host};Port={url.Port};Database={url.LocalPath.Substring(1)};";
            
            if (queryDictionary.AllKeys.Any(e => e == "currentSchema"))
            {
                connectionString = connectionString + $"Search Path={queryDictionary["currentSchema"]};";
            }

            return connectionString;
        }

        /// <summary></summary>
        public static string GetConnectionString(string urlString, string userId, string password, string options="Pooling=true;")
        {
            return $"{Parse(urlString)}User ID={userId};Password={password};{options}";
        }
    }
}