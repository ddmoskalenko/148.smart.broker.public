using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Serilog;

using SmartBroker.Model;
using SmartBroker.DataAccess;
using SmartBroker.Curriculums;


namespace SmartBroker.Services
{

    /// <summary>Сервис </summary>
    public class StudentsControllerAgent : IStudentOperation
    {
        protected readonly ILogger Logger;
        protected readonly StudentDBContext StudentDBContext;
        protected readonly IConfiguration Configuration;
        private readonly CurriculumsService CurriculumsService;
     

        /// <summary></summary>
        public StudentsControllerAgent(
            Logger logger,
            IConfiguration configuration,
            StudentDBContext alumnoDBContext,
            CurriculumsService curriculumsService)
        {
            Logger = logger.GetLog<StudentDBService>();
            Configuration = configuration;
            StudentDBContext = alumnoDBContext;
            CurriculumsService = curriculumsService;
            if (CurriculumsService == null) Logger.Error("CurriculumsService недоступен");
        }

    
        private OperationResult ValidateSpecialtyAndProfile(Operation operation)
        {
            if (CurriculumsService != null)
            {
                var specialities = CurriculumsService.Specialties();
                var profiles = CurriculumsService.Profiles();

                if (string.IsNullOrEmpty(operation.SpecialityCode) || (string.IsNullOrEmpty(operation.SpecialityName)))
                {
                    operation.StatusId = OperationStatus.INVALID_DATA.ToString();
                    return Operation.CreateOperationResultWithOneStep("Не указан код и название направления подготовки", operation, OperationStatus.INVALID_DATA);
                }

                // validate Specialty Code
                var spec = specialities.Where(pro => pro.SpecialityCode.Replace(".", "").Trim() == operation.SpecialityCode.Replace(".", "").Trim()).FirstOrDefault();
                if (spec == null)
                {
                    operation.StatusId = OperationStatus.INVALID_DATA.ToString();
                    string msg = "Неизвестный код направления подготовки";
                    return Operation.CreateOperationResultWithOneStep(msg, operation, OperationStatus.INVALID_DATA);
                }

                // validate SpecialityName
                if (spec.SpecialityName.ToLower().Trim() != operation.SpecialityName.ToLower().Trim())
                {
                    operation.StatusId = OperationStatus.INVALID_DATA.ToString();
                    string msgAboutSpecialtyName = "Для сущность [" + operation.StudentId + "] название направления '" + operation.SpecialityName + "' не совпадает по коду " + operation.SpecialityCode + ", в нашем справочнике это '" + spec.SpecialityName + "'";

                    return Operation.CreateOperationResultWithOneStep(msgAboutSpecialtyName, operation, OperationStatus.INVALID_DATA);
                }

                operation.SpecialityName = spec.SpecialityName;
                operation.SpecialityCode = spec.SpecialityCode;
          
                if (!string.IsNullOrEmpty(operation.ProfileName))
                {
                    if (!profiles.Any(pro => pro.ProfileName.ToLower() == operation.ProfileName.ToLower()))
                    {
                        operation.StatusId = OperationStatus.INVALID_DATA.ToString();
                        string msgProfile = "Указан профиль не содержащийся в справочнике XX " + operation.ProfileName;
                        return Operation.CreateOperationResultWithOneStep(msgProfile, operation, OperationStatus.INVALID_DATA);
                    }
                    else
                    {
                        var dicProfile = profiles.Where(pro => pro.ProfileName.ToLower() == operation.ProfileName.ToLower()).First();
                        if (dicProfile.SpecialityId.Replace(".","") != operation.SpecialityCode.Replace(".",""))
                        {
                            operation.StatusId = OperationStatus.INVALID_DATA.ToString();
                            string msgProfile = "В операции ["+operation.Id+"] указаный профиль '"+operation.ProfileName+"' в справочнике связан с другим направлением подготовки '"+dicProfile.SpecialityId+"' вместо '"+operation.SpecialityCode+"'" ;
                            Logger.Error(msgProfile);
                            return Operation.CreateOperationResultWithOneStep(msgProfile, operation, OperationStatus.INVALID_DATA);
                        }
                    }
                }
            }
            else
            {
                throw new Exception("API XX недоступно");
            }

            var alumno = StudentDBContext.Student.Where(stu => stu.StudentId == operation.StudentId).IgnoreQueryFilters().FirstOrDefault();
            if (alumno == null) throw new Exception("в таблице сущностей не найден сущность с идентификатором [" + operation.StudentId + "] из операции [" + operation.Id + "]");

            alumno.SpecialtyCode = operation.SpecialityCode;
            alumno.SpecialtyName = operation.SpecialityName;
            alumno.SpecialtyProfile = operation.ProfileName;
            StudentDBContext.SaveChanges();

            return Operation.CreateOperationResultWithOneStep("Направление '" + operation.SpecialityCode + "' и профиль корректные '" + operation.ProfileName + "'", operation, OperationStatus.SUCCESSFUL);
        }


     
        public OperationResult StudentEnrollment(Operation operation)
        {
            return ValidateSpecialtyAndProfile(operation);
        }

        public OperationResult ChangingProfile(Operation operation)
        {
            return ValidateSpecialtyAndProfile(operation);
        }

        public OperationResult StudentExpulsion(Operation operation)
        {
            return null;
        }
    
        public OperationResult CancelEnrollment(Operation operation)
        {
            var alumnosToDelete = StudentDBContext.Student.Where(s=>s.StudentId==operation.StudentId).FirstOrDefault(); 
            if (alumnosToDelete!=null)
            {
                StudentDBContext.Student.Remove(alumnosToDelete);
                string msg = "Сущность ["+operation.StudentId+"] удален. Операция ["+operation.Id+"]";
                Logger.Information(msg); 
                return Operation.CreateOperationResultWithOneStep(msg, operation, OperationStatus.SUCCESSFUL);
            }
            else 
            {
                string msg = "Сущность ["+operation.StudentId+"] не найден в таблице сущностей, возможно он уже удален. Операция ["+operation.Id+"]";
                Logger.Warning(msg); 
                return Operation.CreateOperationResultWithOneStep(msg, operation, OperationStatus.WARNING);
            }
        }

        public bool HealthCheck()
        {
            bool success = true; 

            try
            {
                var profil = CurriculumsService.GetProfiles().First().SpecialityId;
                if (string.IsNullOrEmpty(profil)) throw new Exception("Код профиля пустой");
            }
            catch(Exception ex)
            {
                success = false;
                Logger.Error(ex, "CurriculumsService недоступен, HealthCheck не прошел");
            }
      

            return success;


        }
    }
}



