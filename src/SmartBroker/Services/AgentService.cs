using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

using SmartBroker.DataAccess;

using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

using Serilog;



namespace SmartBroker.Services
{//
    /// <summary>Сервис обработки очереди операций</summary>
    internal class AgentService : IHostedService//, IDisposable
    {
        private readonly ILogger Logger;
        protected IConfiguration Configuration;
        IHttpContextAccessor HttpContextAccessor;
        private readonly IWritableAuthenticationContext AuthenticationContext;
        private ServiceUserAuthenticator ServiceUserAuthenticator;
        private IHostingEnvironment Env { get; }
        public IServiceProvider Services { get; }

        public AgentService(IServiceProvider services, Logger logger, IHttpContextAccessor httpContextAccessor,
            IWritableAuthenticationContext authenticationContext,
            ServiceUserAuthenticator serviceUserAuthenticator,
            IHostingEnvironment env)
        {
            Services = services;
            Logger = logger.GetLog<AgentService>();
            AuthenticationContext = authenticationContext;
            HttpContextAccessor = httpContextAccessor;
            ServiceUserAuthenticator = serviceUserAuthenticator;
            Env = env;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Logger.Information("Фоновый процесс для " + CommonSettings.applcation + " стартовал");

            DoWork(cancellationToken);
            return Task.CompletedTask;
        }

        /// <summary>Главный цикл обработки операций по таймеру</summary>
        private async void DoWork(CancellationToken stoppingToken)
        {
            stoppingToken.Register(() => { });
            try
            {
                using (var scope = Services.CreateScope())
                {
                    Configuration = scope.ServiceProvider.GetRequiredService<IConfiguration>();
                    var periodPlanningService = scope.ServiceProvider.GetRequiredService<PeriodPlanningAgent>();
                    int iteration = 0;
                    bool healthCheckSuccessful = false;


                    ProcessSetting processSetting = new ProcessSetting(Configuration);
                    if (Env.IsDevelopment()) processSetting.ProcessingQueueTimeout = 20;

                    while (!stoppingToken.IsCancellationRequested)
                    {
                        Logger.Debug($"Запустился процесс обработки операций");
                        iteration++;
                        using (AuthenticationContext.SetAuthenticationData(ServiceUserAuthenticator))
                        {
                            var scopedDBContext = scope.ServiceProvider.GetRequiredService<StudentDBContext>();
                            var alumnoDBService = scope.ServiceProvider.GetRequiredService<StudentDBService>();
                            var alumnosControllerAgent = scope.ServiceProvider.GetRequiredService<StudentsControllerAgent>();

                            if (!healthCheckSuccessful)
                            {
                                healthCheckSuccessful = alumnosControllerAgent.HealthCheck() && periodPlanningService.HealthCheck();
                                if (healthCheckSuccessful) Logger.Information("Проверка доступности требуемых удаленных сервисов успешна");
                            }

                            List<Operation> operationToProccess = null;
                            try
                            {  // first transaction
                                using (var transaction = scopedDBContext.Database.BeginTransaction())
                                {
                                    // if( iteration % processSetting.ProcessingIterationLimit == 0) alumnoDBService.UnLockLostProcess(processSetting);
                                    operationToProccess = alumnoDBService.LockOperations(processSetting, scopedDBContext);

                                    if (operationToProccess.Count > 0)
                                    {
                                        Logger.Information("Захвачено для выполнения (" + operationToProccess.Count().ToString() + ") операций");

                                        foreach (var operation in operationToProccess.Where(o => o.StatusId == OperationStatus.AWAITING.ToString()))
                                        {
                                            try
                                            {
                                                switch ((OperationType)Enum.Parse(typeof(OperationType), operation.OperationTypeId))
                                                {
                                                    case OperationType.ALUMNO_ENROLLMENT:
                                                        {
                                                            alumnoDBService.SaveOperationResult(scopedDBContext, operation, ServiceAgentType.StudentControllerService, alumnosControllerAgent.StudentEnrollment(operation));

                                                            if (operation.StatusId == OperationStatus.SUCCESSFUL.ToString())
                                                                alumnoDBService.SaveOperationResult(scopedDBContext, operation, ServiceAgentType.PeriodPlanningService, periodPlanningService.StudentEnrollment(operation));
                                                            break;
                                                        }
                                                    case OperationType.CHANGE_PROFILE:
                                                        {
                                                            alumnoDBService.SaveOperationResult(scopedDBContext, operation, ServiceAgentType.StudentControllerService, alumnosControllerAgent.ChangingProfile(operation));

                                                            if (operation.StatusId == OperationStatus.SUCCESSFUL.ToString())
                                                                alumnoDBService.SaveOperationResult(scopedDBContext, operation, ServiceAgentType.PeriodPlanningService, periodPlanningService.ChangingProfile(operation));
                                                            break;
                                                        }
                                                    case OperationType.ALUMNO_EXPULTION:
                                                        {
                                                            alumnoDBService.SaveOperationResult(scopedDBContext, operation, ServiceAgentType.PeriodPlanningService, periodPlanningService.StudentExpulsion(operation));
                                                            break;
                                                        }
                                                    /* выполянется безотлагательно после активации
                                                case OperationType.CANCEL_ENROLLMENT:
                                                    {
                                                        alumnoDBService.SaveOperationResult(operation, ServiceAgentType.PeriodPlanningService, periodPlanningService.CancelEnrollment(operation));
                                                        break;
                                                    } */

                                                    default:
                                                        {
                                                            break;
                                                        }
                                                }
                                                Logger.Information("Операция " + operation.OperationTypeId + " [" + operation.Id + "] выполнена, статус изменен на " + operation.StatusId);

                                            }
                                            catch (System.ArgumentException ae)
                                            {
                                                operation.StatusId = OperationStatus.INVALID_DATA.ToString();
                                                Logger.Error(ae, $"Неизвестный тип события в операции {operation.OperationTypeId}");
                                            }
                                        }
                                    }
                                    else Logger.Debug("Нет операций для обработки");
                                    scopedDBContext.SaveChanges();
                                    transaction.Commit();
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.Error(ex, "Операции прерваны");
                                // alumnoDBService.UnlockOperations(operationToProccess);
                            }

                           
                        }

                        await Task.Delay(1000 * processSetting.ProcessingQueueTimeout, stoppingToken);
                        Logger.Debug("Следующая итерация обработки операций через " + processSetting.ProcessingQueueTimeout.ToString() + " сек.");
                    }
                    Logger.Information($"хост процес " + GetType() + " остановлен");
                }
            }
            catch (Exception ex)
            {
                Logger.Debug(ex, $"Внутри " + GetType() + " возникло исключение");
            }
        }


        /* 
                private void MarkOperationAsProccessed(StudentDBContext scopedDBContext, List<Operation> operations, bool locking = true)
                {
                    if (operations.Count == 0) return;
                    if (locking) operations.ForEach(o => o.StatusId = ResultStatus.PROCESSING.ToString());
                    else operations.ForEach(o => o.StatusId = ResultStatus.AWAITING.ToString());
                }
        */
        public Task StopAsync(CancellationToken cancellationToken)
        {
            Logger.Information("Хост-процесс для " + GetType() + " остановлен");//
            return Task.CompletedTask;
        }
    }

}
