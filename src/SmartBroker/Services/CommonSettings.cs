
namespace SmartBroker.Services
{
    public class CommonSettings
    {
        /// <summary></summary>
        public static string dateformatUtcR = "yyyy-MM-ddTHH:mm:ss.FFFzzzz";    
        public static string dateformatUtcM = "yyyy-MM-ddTHH:mm:ss.FFFzzzz";
        public static string dateformatUtcS = "yyyy-MM-ddTHH:mm:ss.FFF";
        public static string dateformatT = "yyyy-MM-ddTHH:mm:sszzzz";
        public static string dateformat = "yyyy-MM-dd";
        public static string dateformatUI = "dd.MM.yyyy HH:mm";
        public static string applcation = "alumnos-controller";
    }
}