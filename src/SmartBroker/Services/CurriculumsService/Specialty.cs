

namespace SmartBroker.Curriculums
{
    /// <summary>Направления подготовки</summary>
    [NotMapped]
    public class Speciality //: A11 EntityWithDisplayName
    {
        /// <summary>Код (=идентификатор) направления подготовки</summary>
        public string SpecialityCode { get; set; }
         public string SpecialityName { get; set; }
        
    }
}
