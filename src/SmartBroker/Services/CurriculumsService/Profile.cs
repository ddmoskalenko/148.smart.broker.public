using System.ComponentModel.DataAnnotations.Schema;


namespace SmartBroker.Curriculums
{
    /// <summary>Профиль подготовки</summary>
    /// <remarks>Справочник профилей предоставляется сервисом ОП</remarks>
    [NotMapped]
    public class Profile 
    {
        /// <summary>Код  направления подготовки</summary>
        public string SpecialityId { get; set; }
        public string ProfileName { get; set; }
      

    }
}
