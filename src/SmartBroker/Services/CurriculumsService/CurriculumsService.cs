﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Dapper;
using Newtonsoft.Json.Linq;
using Serilog;

//   

namespace SmartBroker.Curriculums
{
    /// <summary>Клиент для сервиса curriculum-description-service</summary>
    public class CurriculumsService
    {
        private readonly IRemoteRestApi RemoteRestApi;
        private readonly ILogger Log;

        private IList<Speciality> specialties = null;
        private IList<Profile> profiles = null;
        
        /// <summary>Конструктор</summary>
        public CurriculumsService(
            IRemoteRestApi remoteRestApi,
            Logger logger)
        {
            if (logger is null) throw new ArgumentNullException(nameof(logger));

            RemoteRestApi = remoteRestApi ?? throw new ArgumentNullException(nameof(remoteRestApi));
            Log = logger.GetLog<CurriculumsService>();
        }


         

        #region Профили
        public IList<Profile> Profiles()
        {
           if (profiles != null) return profiles;
            profiles = GetProfiles();
            return profiles;
        }
    
        public IList<Speciality> Specialties()
        {
           if (specialties != null) return specialties;
            specialties = GetSpecialties();
            return specialties;
        }
 
        /// <summary>Возвращает полный список профилей</summary>
        private IList<Profile> GetProfiles()
        {
            var query = $"mc/references/profiles";
            JArray result = RemoteRestApi.Get<JArray>(query);
            var profiles = new List<Profile>();
            foreach (JObject item in result)
            {
                var profile = new Profile
                {
                    SpecialityId = item.Value<string>("specialtyId"),
                    ProfileName = item.Value<string>("name")
                };

                profiles.Add(profile);
            }
            return profiles;
        }

        /// <summary>Возвращает полный список направлений подготовки</summary>
        private IList<Speciality> GetSpecialties()
        {
            var query = $"mc/references/specialties";
            JArray result = RemoteRestApi.Get<JArray>(query);
            var specialty = new List<Speciality>();
            foreach (JObject item in result)
            {
                var profile = new Speciality
                {
                    SpecialityCode = item.Value<string>("code"),
                    SpecialityName = item.Value<string>("name")
                };

                specialty.Add(profile);
            }
            return specialty;
        }
       
        /// <summary>Возвращает список профилей для указанного направления подготовки</summary>
        public IList<Profile> GetProfiles(string specialityCode)
        {
            return GetProfiles().Where(p => p.SpecialityId == specialityCode).ToList();
        }

        /// <summary>Возвращает список профилей по перечню идентификаторов</summary>
        public IList<Profile> GetProfiles(string[] ids = null)
        {
            return ids == null
                ? GetProfiles()
                : GetProfiles().Where(p => ids.Contains(p.SpecialityId)).ToList();
        }

        /// <summary>Возвращает профиль по идентификатору</summary>
        public Profile GetProfile(string id) => GetProfiles(new[] { id }).Single();

        #endregion

        // Пока не используется, Dapper не поддерживает custom-конвертацию для перечислений
        // https://github.com/StackExchange/Dapper/issues/259
        private class EnumDapperHandler<TEnum> : SqlMapper.TypeHandler<TEnum> where TEnum: struct
        {
            public override TEnum Parse(object value) => ReflectionHelper.ParseEnumByValueCode<TEnum>(value as string);

            public override void SetValue(IDbDataParameter parameter, TEnum value) =>
                // Пока не нужно
                throw new NotImplementedException();
        }
    }
}
