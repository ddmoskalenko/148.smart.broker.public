using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

using SmartBroker.DataAccess;

using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

using Serilog;



namespace SmartBroker.Services
{
    /// <summary>Настройки процессинга</summary>
    public class ProcessSetting
    {
        private const int defaultPackSize = 10;
        private const int defaultProcessingQueueTimeout = 60;
        private const int defaultApplyBeforeHours = 9;
        private const int defaultProcessingIterationLimit = 10;

        public int ProcessingQueueTimeout { get; set; }
        public int PackSize { get; set; }
        public bool PostponeAppling { get; set; }
        public int ApplyBeforeHours { get; set; }
        public int ProcessingIterationLimit  { get; set; }

        public ProcessSetting(IConfiguration Configuration)
        {
            ProcessingQueueTimeout = Configuration.GetIntOrDefault("config:" + CommonSettings.applcation + ":processing.queue.timeout", defaultProcessingQueueTimeout);
            PackSize = Configuration.GetIntOrDefault("config:" + CommonSettings.applcation + ":pack.size", defaultPackSize);
            PostponeAppling = Configuration.GetNotNullBool("config:" + CommonSettings.applcation + ":postpone.appling");
            ApplyBeforeHours = Configuration.GetIntOrDefault("config:" + CommonSettings.applcation + ":apply.before.hours", 0);
            ProcessingIterationLimit = Configuration.GetIntOrDefault("config:" + CommonSettings.applcation + ":processing.iteration.limit", defaultProcessingIterationLimit);
        }

        public ProcessSetting Get()
        {
            if (ProcessingQueueTimeout < defaultPackSize) ProcessingQueueTimeout = defaultProcessingQueueTimeout;
            if (PackSize < 1) PackSize = defaultPackSize;
            return this;
        }
    }
}