using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SmartBroker.Services
{
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class ChangeProgramCommand
    {
        /// <summary>Идентификатор сущность</summary>
        [Required]
        public string StudentId { get; set; }

        /// <summary>Предыдущий идентификатор сущность</summary>
        [Required]
         public string PreviousStudentID { get; set; }

        /// <summary>Предыдущий идентификатор потока обучения</summary>
        [Required]
        public string PreviousCurriculumFlowId { get; set; }

        /// <summary>Предыдущий идентификатор (код) напрfвления подготовки</summary>
        [Required]
        public string PreviousSpecialityId { get; set; }

        /// <summary>Предыдущее название профиля, если есть</summary>
        public string PreviousProfileName { get; set; }
    }
}