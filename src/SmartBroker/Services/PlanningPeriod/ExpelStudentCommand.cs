using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SmartBroker.Services
{
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class ExpelStudentCommand
    {
        /// <summary>Идентификатор сущность</summary>
        [Required]
        public string StudentId { get; set; }
    }
}