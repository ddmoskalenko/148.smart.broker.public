using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;


using Microsoft.AspNet.OData;

using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Serilog;

using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


using SmartBroker.Model;
using SmartBroker.DataAccess;
using SmartBroker.Extensions;
using SmartBroker.Curriculums;



namespace SmartBroker.Services
{

    /// <summary>Сервис работы c XXX на уровне операций</summary>
    public class PeriodPlanningAgent : IStudentOperation
    {
        protected readonly ILogger Logger;
        protected readonly Logger CLogger;
        private readonly IConfiguration Configuration;
        private IHttpContextAccessor HttpContextAccessor;
        private readonly IWritableAuthenticationContext AuthenticationContext;
        private ServiceUserAuthenticator ServiceUserAuthenticator;
   

        public PeriodPlanningAgent(
            Logger logger,
            IConfiguration configuration,
            IHttpContextAccessor httpContextAccessor,
            IWritableAuthenticationContext authenticationContext,
            ServiceUserAuthenticator serviceUserAuthenticator
            )
        {
            Logger = logger.GetLog<PeriodPlanningAgent>();
            Configuration = configuration;
            AuthenticationContext = authenticationContext;
            HttpContextAccessor = httpContextAccessor;
            ServiceUserAuthenticator = serviceUserAuthenticator;
            CLogger = logger;
        }

        public bool HealthCheck()
        {
            const string localUrl = "/api/manage/health-check";
            var option = Configuration.GetSection("config:" + CommonSettings.applcation).ToRestApiConnectionOptions("remote-api-url.periodplanning");
       
            var restAPI = new RemoteRestApiFactory(CommonSettings.applcation, AuthenticationContext, HttpContextAccessor, CLogger).Create(option);

            try
            {
                var result = restAPI.Get<string>(localUrl);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "API недоступно, HealthCheck("+localUrl +") не прошел");
                return false;
            }
            return true;

        }

        public OperationResult StudentEnrollment(Operation operation)
        {
            string localUrl = "alumno-movements/enroll-alumno";
            var option = Configuration.GetSection("config:" + CommonSettings.applcation).ToRestApiConnectionOptions("remote-api-url.periodplanning");
       
            RemoteRestApiFactory factory = new RemoteRestApiFactory(CommonSettings.applcation, AuthenticationContext, HttpContextAccessor, CLogger);
            var restAPI = factory.Create(option);

            var json = JsonConvert.SerializeObject(new { StudentId = operation.StudentId });
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, RestApiHelper.JsonContentType);
            // вызов API 
            try
            {
                var result = restAPI.Post<OperationResult>(localUrl, stringContent);
                if (!result.Success) operation.StatusId = OperationStatus.WARNING.ToString();
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Исключение при выполнении операции ["+operation.Id+"] типа "+operation.OperationTypeId);
            }
            return null;
        }

        public OperationResult StudentExpulsion(Operation operation)
        {
            string localUrl = "alumno-movements/expel-alumno";
            var option = Configuration.GetSection("config:" + CommonSettings.applcation).ToRestApiConnectionOptions("remote-api-url.periodplanning");
  
            RemoteRestApiFactory factory = new RemoteRestApiFactory(CommonSettings.applcation, AuthenticationContext, HttpContextAccessor, CLogger);
            var restAPI = factory.Create(option);

            var json = JsonConvert.SerializeObject( new ExpelStudentCommand() { StudentId = operation.StudentId });
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, RestApiHelper.JsonContentType);
            // вызов API 
            try
            {
                var result = restAPI.Post<OperationResult>(localUrl, stringContent);
                if (!result.Success) operation.StatusId = OperationStatus.WARNING.ToString();       
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Исключение при выполнении операции ["+operation.Id+"] типа "+operation.OperationTypeId);
            }
            return null;
        }

        public OperationResult ChangingProfile(Operation operation)
        {
            OperationResult result = null;
            string localUrl = "alumno-movements/change-alumno-program";
      
            var option = Configuration.GetSection("config:" + CommonSettings.applcation).ToRestApiConnectionOptions("remote-api-url.periodplanning");

            RemoteRestApiFactory factory = new RemoteRestApiFactory(CommonSettings.applcation, AuthenticationContext, HttpContextAccessor, CLogger);
            var restAPI = factory.Create(option);
            var json = JsonConvert.SerializeObject( new ChangeProgramCommand() { StudentId = operation.StudentId, PreviousCurriculumFlowId = operation.PreviousCurriculumFlowId, PreviousProfileName = operation.PreviousProfileName, PreviousSpecialityId = operation.PreviousSpecialityCode, PreviousStudentID = operation.PreviousStudentID });
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, RestApiHelper.JsonContentType);

            // вызов API 
            try
            {
                result = restAPI.Post<OperationResult>(localUrl, stringContent);
                if (!result.Success) operation.StatusId = OperationStatus.WARNING.ToString();       
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Исключение при выполнении операции ["+operation.Id+"] типа "+operation.OperationTypeId);
            }
            return result;
        }

        public OperationResult CancelEnrollment(Operation operation)
        {
            Logger.Information("Выполняем ОтменуЗачисления");
 
            string localUrl = "alumno-movements/delete-alumno-data";
            var option = Configuration.GetSection("config:" + CommonSettings.applcation).ToRestApiConnectionOptions("remote-api-url.periodplanning");


            RemoteRestApiFactory factory = new RemoteRestApiFactory(CommonSettings.applcation, AuthenticationContext, HttpContextAccessor, CLogger);
            var restAPI = factory.Create(option);

            var json = JsonConvert.SerializeObject( new DeleteStudentCommand() { StudentId = operation.StudentId });
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            // вызов 
            try
            {
                var result = restAPI.Post<OperationResult>(localUrl, stringContent);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Исключение при выполнении операции ["+operation.Id+"] типа "+operation.OperationTypeId);
            }
            return null;
        }
    }
}



