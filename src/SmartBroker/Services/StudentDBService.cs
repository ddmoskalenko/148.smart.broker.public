using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Serilog;

using SmartBroker.Model;
using SmartBroker.DataAccess;
using SmartBroker.Curriculums;


namespace SmartBroker.Services
{
    /// <summary>Сервис сохранения входящих событий в очередь</summary>
    public class StudentDBService : IStudentsOperation
    {
        protected readonly ILogger Logger;
        protected readonly StudentDBContext StudentDBContext;
        protected readonly IConfiguration Configuration;
        private readonly CurriculumsService CurriculumsService;
        protected readonly PeriodPlanningAgent PeriodPlanningAgent;
        private readonly StudentsControllerAgent StudentsControllerAgent;


        /// <summary></summary>
        public StudentDBService(
            Logger logger,
            IConfiguration configuration,
            StudentDBContext alumnoDBContext,
            PeriodPlanningAgent periodPlanningAgent,
            CurriculumsService curriculumsService,
            StudentsControllerAgent alumnosControllerAgent)
        {
            Logger = logger.GetLog<StudentDBService>();
            Configuration = configuration;
            StudentDBContext = alumnoDBContext;
            CurriculumsService = curriculumsService;
            PeriodPlanningAgent = periodPlanningAgent;
            StudentsControllerAgent = alumnosControllerAgent;
            if (CurriculumsService == null) Logger.Error("CurriculumsService недоступен");
        }

        /// <summary> Регистрация списка операций на создание нового сущность</summary>
        public EventResult StudentEnrollment(StudentArrow arrow)
        {
            if (StudentDBContext == null)
            {
                return new EventResult("База данных недоступна") { PacketState = PacketState.NotSure, ProcessingStatus = ProcessingStatus.Pending };
            }

            var eventID = Event.CreateEvent(StudentDBContext, OperationType.ALUMNO_ENROLLMENT, arrow, Logger)?.Id;
            if (string.IsNullOrEmpty(eventID))
            {
                return new EventResult("Не создана запись о событии") { PacketState = PacketState.NotSure, ProcessingStatus = ProcessingStatus.Pending };
            }

            try
            {
                Student[] alumnos = arrow.Students.Select(s => new Student()
                {
                    StudentId = s.ID,
                    Id = s.ID,
                    Version = 0,
                    PersonId = s.PersonID,
                    CurriculumFlowId = s.FlowID,
                    LearningStartDate = string.IsNullOrEmpty(s.ApplyDate) ? DateTime.Now : DateTime.ParseExact(s.ApplyDate, CommonSettings.dateformat, new System.Globalization.CultureInfo("en-US")),
                    LearningEndDate = null,
                    LearningEndReason = null,
                }).ToArray();

                StudentDBContext.Student.AddRange(alumnos);
                Operation.CreateOperation(StudentDBContext, OperationType.ALUMNO_ENROLLMENT, arrow.Students.Select(se => new Operation(se, Logger)).ToList(), OperationStatus.AWAITING, eventID, Logger);

                StudentDBContext.SaveChanges();

                return new EventResult() { };
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Ошибка при сохранение сущностей в базе");
                return new EventResult("Ошибка при сохранение сущностей в базе " + ex.Message);
            }
        }

        /// <summary> Регистрация списка операций на ИзменениеПрограммыОбучения</summary>
        public EventResult ChangingProfile(StudentArrow arrowGeneral)
        {
            if (StudentDBContext == null)
            {
                return new EventResult("База данных недоступна") { PacketState = PacketState.NotSure, ProcessingStatus = ProcessingStatus.Pending };
            }

            int toQueue = 0;

            // version of changing without changing ID of alumno
            var arrowSameIDStudents = arrowGeneral.Students.Where(s => string.IsNullOrEmpty(s.IDPrevious) || s.IDPrevious == s.ID ).ToArray();
            if (arrowSameIDStudents.Any())
            {
                StudentArrow arrowSameID = new StudentArrow(arrowGeneral) { Students = arrowSameIDStudents };
                var eventID = Event.CreateEvent(StudentDBContext, OperationType.CHANGE_PROFILE, arrowSameID, Logger)?.Id;
                if (string.IsNullOrEmpty(eventID)) return new EventResult("Не создана запись о событии") { PacketState = PacketState.NotSure, ProcessingStatus = ProcessingStatus.Pending };


                var previousStudents = StudentDBContext.Student.Where(d => arrowSameID.Students.Select(e => e.ID).Contains(d.StudentId)).ToList();
                var notFounded = arrowSameID.Students.Where(e => !previousStudents.Select(p => p.StudentId).Contains(e.ID)).Select(ee => new Operation(ee, Logger) { }).ToList();
                if (notFounded.Count() > 0)
                {
                    Operation.CreateOperationsWithResult(StudentDBContext, OperationType.CHANGE_PROFILE, notFounded, OperationStatus.INVALID_DATA, eventID, ServiceAgentType.StudentControllerService.ToString(), false);
                    string message = "Сущность с таким ID не найден в базе сущностей";
                    Logger.Error(message);
                }

                var alreadyExpulled = arrowSameIDStudents.Where(s=> StudentDBContext.Student.Where(e=>e.LearningEndDate != null).Select(e=>e.StudentId).Contains(s.ID)).Select(ee => new Operation(ee, Logger) { }).ToList();
                if (alreadyExpulled.Count() > 0)
                {
                    string message = "Сущности уже отчисленны ["+string.Join(",",alreadyExpulled.Select(a=>a.StudentId))+"]";
                    StepResult step = new StepResult() { StepName = "Проверка состояния сущность", Description = message, StepStatus = OperationStatus.BUSINESS_RULE_VIOLATED.ToString() };
                    Operation.CreateOperationsWithResult(StudentDBContext, OperationType.CHANGE_PROFILE, alreadyExpulled, OperationStatus.INVALID_DATA, eventID, ServiceAgentType.StudentControllerService.ToString(), false, step);
                    Logger.Error(message);
                }

                var absolutlyTheSame = arrowSameID.Students.Where(e => previousStudents.Select(p => p.StudentId + p.CurriculumFlowId + p.SpecialtyCode?.Replace(".", "") + p.SpecialtyProfile).Contains(e.ID + e.FlowID + e.SpecialtyCode.Replace(".", "") + e.Profile))
                .Select(s => new Operation(s)).ToList();
                if (absolutlyTheSame.Count() > 0)
                {
                    string message = "Сущность уже имeет именно этот прoфиль и напрaвление";
                    StepResult step = new StepResult() { StepName = "Сверка данных", Description = message, StepStatus = OperationStatus.WARNING.ToString() };

                    Operation.CreateOperationsWithResult(StudentDBContext, OperationType.CHANGE_PROFILE, absolutlyTheSame, OperationStatus.WARNING, eventID, ServiceAgentType.StudentControllerService, false, step);
                    Logger.Information(message);
                }
            
                Dictionary<string, StudentEntity> alumnosToProccess
                = arrowSameID.Students.Where(e => !absolutlyTheSame.Select(o => o.StudentId).Contains(e.ID) &&
                                                  !alreadyExpulled.Select(o => o.StudentId).Contains(e.ID)  
                                            ).Select(e => e).ToDictionary(ee => ee.ID);


                List<Operation> operations = new List<Operation>();
                foreach (string alumnoID in alumnosToProccess.Keys)
                {
                    var alumnoDB = previousStudents.Where(ps => ps.StudentId == alumnoID).First();
                    var operation = new Operation(alumnosToProccess[alumnoID], Logger);
                    operation.SetPrevious(alumnoDB);

                    alumnoDB.CurriculumFlowId = alumnosToProccess[alumnoID].FlowID;//?????
                    alumnoDB.LearningStartDate = operation.ApplyDate;

                    operations.Add(operation);
                }
                Operation.CreateOperation(StudentDBContext, OperationType.CHANGE_PROFILE, operations, OperationStatus.AWAITING, eventID, Logger);
                toQueue  = operations.Count;
            }

            var arrowAnotherIDStudents = arrowGeneral.Students.Where(s => !string.IsNullOrEmpty(s.IDPrevious)).ToArray();
            if (arrowAnotherIDStudents.Any())
            {// этот вариант допускает что пред сущность  не существует
                Logger.Debug("Событие ["+arrowGeneral.EventID+"] XXX содержит ("+arrowAnotherIDStudents.Count().ToString()+") сущностей ["+string.Join(",", arrowAnotherIDStudents.Select(s=>s.ID))+"], у которых изменился ID");
                StudentArrow arrowAnotherID = new StudentArrow(arrowGeneral) { Students = arrowAnotherIDStudents };
                var eventID = Event.CreateEvent(StudentDBContext, OperationType.CHANGE_PROFILE, arrowAnotherID, Logger)?.Id;
                if (string.IsNullOrEmpty(eventID)) return new EventResult("Не создана запись о событии") { PacketState = PacketState.NotSure, ProcessingStatus = ProcessingStatus.Pending };


                var previousStudents = StudentDBContext.Student.Where(d => arrowAnotherID.Students.Select(e => e.IDPrevious).Contains(d.StudentId)).ToList();

                var notFounded = arrowAnotherID.Students.Where(e => !previousStudents.Select(p => p.StudentId).Contains(e.IDPrevious)).Select(ee => new Operation(ee, Logger) { }).ToList();
                if (notFounded.Count() > 0)
                {
                    Operation.CreateOperationsWithResult(StudentDBContext, OperationType.CHANGE_PROFILE, notFounded, OperationStatus.INVALID_DATA, eventID, ServiceAgentType.StudentControllerService.ToString(), false);
                    string message = "Сущность с таким предыдущим ID не найден в базе сущностей";
                    Logger.Error(message);
                    // remove no founded prev
                    arrowAnotherID.Students = arrowAnotherID.Students.Where(a=> !notFounded.Select(n=>n.StudentId).Contains(a.ID)).ToArray();

                }

                var notSamePerson = arrowAnotherID.Students.Where(e => previousStudents.Where(p => p.StudentId == e.IDPrevious && p.PersonId!=e.PersonID).Any()).Select(ee => new Operation(ee, Logger) { StudentId = ee.IDPrevious }).ToList();
                if (notSamePerson.Count() > 0)
                {
                
                    string message = "Предыдущий сущность, связан с физлицом, отличными от переданного в пакете";
                    Operation.CreateOperationsWithResult(StudentDBContext, OperationType.CHANGE_PROFILE, notSamePerson, OperationStatus.INVALID_DATA, eventID, ServiceAgentType.StudentControllerService.ToString(), false, new StepResult(){ Description = message });
              
                    Logger.Error(message);
                    arrowAnotherID.Students = arrowAnotherID.Students.Where(a=> !notSamePerson.Select(n=>n.StudentId).Contains(a.ID)).ToArray();
                }

                // создадим новых сущностей
                Student[] alumnos = arrowAnotherID.Students.Select(s => new Student()
                {
                    StudentId = s.ID,
                    Id = s.ID,
                    Version = 0,
                    PersonId = s.PersonID,
                    CurriculumFlowId = s.FlowID,
                    LearningStartDate = string.IsNullOrEmpty(s.ApplyDate) ? DateTime.Now : DateTime.ParseExact(s.ApplyDate, CommonSettings.dateformat, new System.Globalization.CultureInfo("en-US")),
                    LearningEndDate = null,
                    LearningEndReason = null,
                }).ToArray();
                StudentDBContext.Student.AddRange(alumnos);
               

                Dictionary<string, StudentEntity> alumnosToProccess = arrowAnotherID.Students.Select(e => e).ToDictionary(ee => ee.IDPrevious);

                List<Operation> operations = new List<Operation>();
                foreach (string alumnoID in alumnosToProccess.Keys)
                {
                    if (string.IsNullOrEmpty(alumnoID))
                    {
                         Logger.Error("Не найден предыдущий сущность из "+string.Join(",",arrowAnotherID.Students.Select(e => e.ID)));
                         throw new NotImplementedException();
                    }
                    var previousStudent = previousStudents.Where(ps => ps.StudentId == alumnoID).First();
                    var operation = new Operation(alumnosToProccess[alumnoID], Logger);
                    //Logger.Debug("operation ID ["+operation.Id+"], alumnoID ["+previuousStudent.StudentId+"]");
                    operation.SetPrevious(previousStudent);

                    previousStudent.LearningEndDate   = operation.ApplyDate;
                    previousStudent.LearningEndReason = alumnosToProccess[alumnoID].RegistryReason;
                    
                    operations.Add(operation);
                }
                Operation.CreateOperation(StudentDBContext, OperationType.CHANGE_PROFILE, operations, OperationStatus.AWAITING, eventID, Logger);
                toQueue = toQueue + operations.Count;
            }


            StudentDBContext.SaveChanges();

            return new EventResult(toQueue);
        }

        /// <summary> Регистрация списка операций на Исключение сущность</summary>
        public EventResult StudentExpulsion(StudentArrow arrow)
        {
            Logger.Error("Вызван StudentExpulsion на "+arrow.Students.Count()+" сущностей");
            
            var eventID = Event.CreateEvent(StudentDBContext, OperationType.ALUMNO_EXPULTION, arrow, Logger)?.Id;
            if (string.IsNullOrEmpty(eventID)) Logger.Information("Не создан идентификатор");

            var previousStudents = StudentDBContext.Student.Where(d => arrow.Students.Select(e => e.ID).Contains(d.StudentId)).ToList();
            var notFounded = arrow.Students.Where(e => !previousStudents.Select(p => p.StudentId).Contains(e.ID)).Select(ee => new Operation(ee, Logger) { }).ToList();
            if (notFounded.Count() > 0)
            {
                Operation.CreateOperationsWithResult(StudentDBContext, OperationType.ALUMNO_EXPULTION, notFounded, OperationStatus.INVALID_DATA, eventID, ServiceAgentType.StudentControllerService, false);
                string message = "Сущности с таким ID [" + string.Join(",", notFounded.Select(o => o.StudentId)) + "] не найдены в базе сущностей";
                // operation.AddStep(message);
                Logger.Information(message);
            }


            Dictionary<string, StudentEntity> alumnosToProccess
              = arrow.Students.Where(e => !notFounded.Select(o => o.StudentId).Contains(e.ID)).Select(e => e).ToDictionary(ee => ee.ID);

            List<Operation> operations = new List<Operation>();
            foreach (string alumnoID in alumnosToProccess.Keys)
            {
                var operation = new Operation(alumnosToProccess[alumnoID], Logger);
                var alumnoDB = previousStudents.Where(ps => ps.StudentId == alumnoID).First();

                alumnoDB.LearningEndDate = operation.ApplyDate;
                alumnoDB.LearningEndReason = operation.Reason;

                operations.Add(operation);
            }

            Operation.CreateOperation(StudentDBContext, OperationType.ALUMNO_EXPULTION, operations, OperationStatus.AWAITING, eventID, Logger);

            StudentDBContext.SaveChanges();
            //end of transaction

            return new EventResult();
        }

        public EventResult CancelEnrollment(StudentArrow arrow)
        {
            var eventID = Event.CreateEvent(StudentDBContext, OperationType.CANCEL_ENROLLMENT, arrow, Logger)?.Id;
            if (string.IsNullOrEmpty(eventID))
            {
                return new EventResult("Не создан идентификатор события ");
                // Logger.Error("Не создан идентификатор события ");
            }

            //startTrans
            var previousStudents = StudentDBContext.Student.Where(d => arrow.Students.Select(e => e.ID).Contains(d.StudentId)).ToList();
            var notFounded = arrow.Students.Where(e => !previousStudents.Select(p => p.StudentId).Contains(e.ID)).Select(ee => new Operation(ee, Logger) { }).ToList();
            if (notFounded.Count() > 0)
            {
                Operation.CreateOperationsWithResult(StudentDBContext, OperationType.CHANGE_PROFILE, notFounded, OperationStatus.INVALID_DATA, eventID, ServiceAgentType.StudentControllerService, false);
                string message = "Сущность с таким ID не найден в базе сущностей [" + string.Join(",", notFounded) + "]";
                //Logger.Error(message);
                return new EventResult(message);
            }

            Dictionary<string, StudentEntity> alumnosToProccess
            = arrow.Students.Where(e => !notFounded.Select(o => o.StudentId).Contains(e.ID)).Select(e => e).ToDictionary(ee => ee.ID);

            List<Operation> operations = new List<Operation>();
            foreach (string alumnoID in alumnosToProccess.Keys)
            {
                var operation = new Operation(alumnosToProccess[alumnoID]);
                operations.Add(operation);
            }

            Operation.CreateOperation(StudentDBContext, OperationType.CANCEL_ENROLLMENT, operations, OperationStatus.SUSPENDING, eventID, Logger);
            Operation.SuspendForStudent(StudentDBContext, operations);

            StudentDBContext.SaveChanges();
            //end of transaction

            return new EventResult();
        }

   

        /// <summary>Захват операций на обработку</summary>
        public List<Operation> LockOperations(ProcessSetting setting, StudentDBContext db)
        {
            List<Operation> operationToProccess = new List<Operation>();
            string sql = string.Empty;
            if (setting.PostponeAppling)
            {
                var peakDate = DateTime.Now.AddHours(-1 * setting.ApplyBeforeHours).ToString(CommonSettings.dateformatUI);
                sql = "SELECT * FROM Operation where id in ( SELECT id FROM Operation WHERE apply_date<=to_timestamp('" + peakDate + "','DD.MM.YYYY HH24:MI') AND status_id='" + OperationStatus.AWAITING + "' AND alumno_id not in (select alumno_id from operation where status_id='" + OperationStatus.PROCESSING + "') order by aud_created_ts LIMIT " + setting.PackSize.ToString() + ") order by alumno_id, apply_date FOR UPDATE SKIP LOCKED";
            }
            else sql = "SELECT * FROM Operation where status_id='" + OperationStatus.AWAITING + "' and alumno_id in ( SELECT alumno_id FROM Operation WHERE status_id='" + OperationStatus.AWAITING + "' AND alumno_id not in (select alumno_id from operation where status_id='" + OperationStatus.PROCESSING + "') order by aud_created_ts LIMIT " + setting.PackSize.ToString() + ") order by alumno_id, aud_created_ts FOR UPDATE SKIP LOCKED";
            operationToProccess = db.Operation.FromSql(sql).ToList();

            db.SaveChanges();
            return operationToProccess;
        }

        private List<Operation> ActivateSuspendedOperation(List<Operation> operations)
        {
            var alsoSupended = StudentDBContext.Operation.Where(o => operations.Select(a => a.StudentId).Contains(o.StudentId) && o.StatusId == OperationStatus.SUSPENDING.ToString() && o.OperationTypeId != OperationType.CANCEL_ENROLLMENT.ToString()).ToList();
            alsoSupended.ForEach(op => op.StatusId = OperationStatus.AWAITING.ToString());
            return alsoSupended;
        }

        /// <summary>Игнорирует переданные ОтменыЗачисления, а все операциии последующие выполняются</summary>
        public string CancelOperation(string[] operations)
        {
            // разрешены только отмены отменЗачисления
            var allowed = StudentDBContext.Operation.Where(d => d.OperationTypeId == OperationType.CANCEL_ENROLLMENT.ToString()
                                                               && operations.Contains(d.Id)
                                                               && (d.StatusId == OperationStatus.SUSPENDING.ToString() || d.StatusId == OperationStatus.AWAITING.ToString())).ToList();

            allowed.ForEach(op => op.StatusId = OperationStatus.CANCELED.ToString());
            var notallowed = operations.Where(op => !allowed.Select(a => a.Id).Contains(op));

            var alsoSupended = ActivateSuspendedOperation(allowed);

            StudentDBContext.SaveChanges();

            if (allowed.Any()) Logger.Information("Отмена произведена для опeраций [" + string.Join(",", allowed.Select(a => a.Id)) + "]");
            if (notallowed.Any()) Logger.Information("Опeрации [" + string.Join(",", notallowed) + "] не могут быть отменены");
            if (alsoSupended.Any()) Logger.Information("Приостановленные опeрации [" + string.Join(",", alsoSupended.Select(o => o.Id)) + "] операции переведены в статус " + OperationStatus.AWAITING.ToString());
            return string.Empty;
        }

        /// <summary>После выбора активации отменуЗачисления все операциии последующие отменяются</summary>     
        public string ActivateOperation(string[] operations)
        {
            // разрешены только для остановленных Отмен
            var allowedActivateEnrollment = StudentDBContext.Operation.Where(d => d.OperationTypeId == OperationType.CANCEL_ENROLLMENT.ToString()
                                                               && operations.Contains(d.Id)
                                                               && (d.StatusId == OperationStatus.SUSPENDING.ToString()));
            if (!allowedActivateEnrollment.Any())
            {
                Logger.Information("Отмен не найдено, активация не выполнена");
            }

            foreach (var operation in allowedActivateEnrollment)
            {
                this.SaveOperationResult(StudentDBContext, operation, ServiceAgentType.PeriodPlanningService, PeriodPlanningAgent.CancelEnrollment(operation));
                CancelLaterOperation(operation);
                // this.SaveOperationResult(operation, ServiceAgentType.StudentControllerService, this.CancelEnrollment(operation));
                this.SaveOperationResult(StudentDBContext, operation, ServiceAgentType.StudentControllerService, StudentsControllerAgent.CancelEnrollment(operation));
            }

            StudentDBContext.SaveChanges();
            if (allowedActivateEnrollment.Any()) Logger.Information("Отмена выполнена для опeраций [" + string.Join(",", allowedActivateEnrollment.Select(a => a.Id)) + "]");
            return string.Empty;
        }

        /// <summary>после активированной отмены зачисления все операциии последующие отменяются</summary>
        private void CancelLaterOperation(Operation operation)
        {
            var cancelOperation = StudentDBContext.Operation.Where(d => d.OperationTypeId != OperationType.CANCEL_ENROLLMENT.ToString()
                                                               && operation.StudentId == d.StudentId
                                                               && (d.StatusId == OperationStatus.SUSPENDING.ToString())).ToList();
            cancelOperation.ForEach(op => op.StatusId = OperationStatus.CANCELED.ToString());
        }
     

        /// <summary>Сохранение ответа о результатах операции</summary>

        public void SaveOperationResult(StudentDBContext db, Operation operation, string serviceID, OperationResult operationResult)
        {
            if (operationResult == null)
            {
                operation.StatusId = OperationStatus.EXCEPTION.ToString();
                operationResult = new OperationResult() { Success = false, ServiceAgentId = serviceID, OperationId = operation.Id };
            }
            else
            {
                operationResult.OperationId = operation.Id;
                operationResult.ServiceAgentId = serviceID;
                operationResult.Numerate();
                operationResult.StepResults.ForEach(s => s.OperationResultId = operationResult.Id);
                db.StepResult.AddRange(operationResult.StepResults);
                if (operationResult.Success && !db.OperationResult.Where(op => op.OperationId == operation.Id && op.Success == false).Any()) operation.StatusId = OperationStatus.SUCCESSFUL.ToString();
                else if (operation.StatusId == string.Empty) operation.StatusId = OperationStatus.WARNING.ToString();
            }
            db.OperationResult.Add(operationResult);
        }
    }
}



