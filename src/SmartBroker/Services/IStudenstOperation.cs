using System.Collections.Generic;
using System.Threading.Tasks;
using SmartBroker.Model;

namespace SmartBroker.Services
{
    public interface IStudentsOperation
    {
        EventResult StudentEnrollment(StudentArrow alumnos);
        
        EventResult StudentExpulsion(StudentArrow alumnos);
        
        EventResult ChangingProfile(StudentArrow alumnos);
        
        EventResult CancelEnrollment(StudentArrow alumnos);
    }
}