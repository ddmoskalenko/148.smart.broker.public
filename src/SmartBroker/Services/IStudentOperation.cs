using System.Collections.Generic;
using System.Threading.Tasks;
using SmartBroker.Model;

using SmartBroker.DataAccess;


namespace SmartBroker.Services
{
    public interface IStudentOperation
    {
        OperationResult StudentEnrollment(Operation operation);
        
        OperationResult StudentExpulsion(Operation operation);
        
        OperationResult ChangingProfile(Operation operation);
  
        OperationResult CancelEnrollment(Operation operation);

        bool HealthCheck();
    }
}